package org.labs.lab1.task2;

import org.junit.Test;
import org.junit.BeforeClass;
import org.junit.Assert;

/**
 * Created by Слава on 12.03.2017.
 */
public class SuperBucketSortTest {

    private static SuperBucketSort bucketSort;
    private static Integer[] array = {
            649, 693, 404, 34, 430, 404, 999
    };
    private static final String[] standard_text = {
            "Max value is 999",
            "Create 7 buckets",
            "Add 649", "In 4 bucket", "After adding. This bucket contains: [649]",
            "Add 693", "In 4 bucket", "Before adding. This bucket contains: [649]", "After adding. This bucket contains: [649, 693]",
            "Add 404", "In 2 bucket", "After adding. This bucket contains: [404]",
            "Add 34", "In 0 bucket", "After adding. This bucket contains: [34]",
            "Add 430", "In 3 bucket", "After adding. This bucket contains: [430]",
            "Add 404", "In 2 bucket", "Before adding. This bucket contains: [404]", "After adding. This bucket contains: [404, 404]",
            "Add 999", "In 6 bucket", "After adding. This bucket contains: [999]",
            "Get 34", "Of 0 bucket", "Set this value on 0 index",
            "Get 404", "Of 2 bucket", "Set this value on 1 index",
            "Get 404", "Of 2 bucket", "Set this value on 2 index",
            "Get 430", "Of 3 bucket", "Set this value on 3 index",
            "Get 649", "Of 4 bucket", "Set this value on 4 index",
            "Get 693", "Of 4 bucket", "Set this value on 5 index",
            "Get 999", "Of 6 bucket", "Set this value on 6 index",
            "Array after sort [34, 404, 404, 430, 649, 693, 999]"
    };

    @BeforeClass
    public static void initTest() {
        bucketSort = new SuperBucketSort();
        bucketSort.bucket(array);

        for(String str : standard_text) {
            System.out.println(str);
        }
    }

    @Test
    public void test_max_value() throws Exception {
        Assert.assertEquals(standard_text[0], bucketSort.log.get(0));
    }

    @Test
    public void test_buckets_number() throws Exception {
        Assert.assertEquals(standard_text[1], bucketSort.log.get(1));
    }

    @Test
    public void test_add_value_1() throws Exception {
        Assert.assertEquals(standard_text[2], bucketSort.log.get(2));
    }

    @Test
    public void test_in_bucket_1() throws Exception {
        Assert.assertEquals(standard_text[3], bucketSort.log.get(3));
    }

    @Test
    public void test_after_adding_1() throws Exception {
        Assert.assertEquals(standard_text[4], bucketSort.log.get(4));
    }

    @Test
    public void test_add_value_2() throws Exception {
        Assert.assertEquals(standard_text[5], bucketSort.log.get(5));
    }

    @Test
    public void test_in_bucket_2() throws Exception {
        Assert.assertEquals(standard_text[6], bucketSort.log.get(6));
    }

    @Test
    public void test_before_adding_2() throws Exception {
        Assert.assertEquals(standard_text[7], bucketSort.log.get(7));
    }

    @Test
    public void test_after_adding_2() throws Exception {
        Assert.assertEquals(standard_text[8], bucketSort.log.get(8));
    }

    @Test
    public void test_add_value_3() throws Exception {
        Assert.assertEquals(standard_text[9], bucketSort.log.get(9));
    }

    @Test
    public void test_in_bucket_3() throws Exception {
        Assert.assertEquals(standard_text[10], bucketSort.log.get(10));
    }

    @Test
    public void test_after_adding_3() throws Exception {
        Assert.assertEquals(standard_text[11], bucketSort.log.get(11));
    }

    @Test
    public void test_add_value_4() throws Exception {
        Assert.assertEquals(standard_text[12], bucketSort.log.get(12));
    }

    @Test
    public void test_in_bucket_4() throws Exception {
        Assert.assertEquals(standard_text[13], bucketSort.log.get(13));
    }

    @Test
    public void test_after_adding_4() throws Exception {
        Assert.assertEquals(standard_text[14], bucketSort.log.get(14));
    }

    @Test
    public void test_add_value_5() throws Exception {
        Assert.assertEquals(standard_text[15], bucketSort.log.get(15));
    }

    @Test
    public void test_in_bucket_5() throws Exception {
        Assert.assertEquals(standard_text[16], bucketSort.log.get(16));
    }

    @Test
    public void test_after_adding_5() throws Exception {
        Assert.assertEquals(standard_text[17], bucketSort.log.get(17));
    }

    @Test
    public void test_add_value_6() throws Exception {
        Assert.assertEquals(standard_text[18], bucketSort.log.get(18));
    }

    @Test
    public void test_in_bucket_6() throws Exception {
        Assert.assertEquals(standard_text[19], bucketSort.log.get(19));
    }

    @Test
    public void test_before_adding_6() throws Exception {
        Assert.assertEquals(standard_text[20], bucketSort.log.get(20));
    }

    @Test
    public void test_after_adding_6() throws Exception {
        Assert.assertEquals(standard_text[21], bucketSort.log.get(21));
    }

    @Test
    public void test_add_value_7() throws Exception {
        Assert.assertEquals(standard_text[22], bucketSort.log.get(22));
    }

    @Test
    public void test_in_bucket_7() throws Exception {
        Assert.assertEquals(standard_text[23], bucketSort.log.get(23));
    }

    @Test
    public void test_after_adding_7() throws Exception {
        Assert.assertEquals(standard_text[24], bucketSort.log.get(24));
    }

    @Test
    public void test_get_value_1() throws Exception {
        Assert.assertEquals(standard_text[25], bucketSort.log.get(25));
    }

    @Test
    public void test_of_bucket_1() throws Exception {
        Assert.assertEquals(standard_text[26], bucketSort.log.get(26));
    }

    @Test
    public void test_set_on_index_1() throws Exception {
        Assert.assertEquals(standard_text[27], bucketSort.log.get(27));
    }

    @Test
    public void test_get_value_2() throws Exception {
        Assert.assertEquals(standard_text[28], bucketSort.log.get(28));
    }

    @Test
    public void test_of_bucket_2() throws Exception {
        Assert.assertEquals(standard_text[29], bucketSort.log.get(29));
    }

    @Test
    public void test_set_on_index_2() throws Exception {
        Assert.assertEquals(standard_text[30], bucketSort.log.get(30));
    }

    @Test
    public void test_get_value_3() throws Exception {
        Assert.assertEquals(standard_text[31], bucketSort.log.get(31));
    }

    @Test
    public void test_of_bucket_3() throws Exception {
        Assert.assertEquals(standard_text[32], bucketSort.log.get(32));
    }

    @Test
    public void test_set_on_index_3() throws Exception {
        Assert.assertEquals(standard_text[33], bucketSort.log.get(33));
    }

    @Test
    public void test_get_value_4() throws Exception {
        Assert.assertEquals(standard_text[34], bucketSort.log.get(34));
    }

    @Test
    public void test_of_bucket_4() throws Exception {
        Assert.assertEquals(standard_text[35], bucketSort.log.get(35));
    }

    @Test
    public void test_set_on_index_4() throws Exception {
        Assert.assertEquals(standard_text[36], bucketSort.log.get(36));
    }

    @Test
    public void test_get_value_5() throws Exception {
        Assert.assertEquals(standard_text[37], bucketSort.log.get(37));
    }

    @Test
    public void test_of_bucket_5() throws Exception {
        Assert.assertEquals(standard_text[38], bucketSort.log.get(38));
    }

    @Test
    public void test_set_on_index_5() throws Exception {
        Assert.assertEquals(standard_text[39], bucketSort.log.get(39));
    }

    @Test
    public void test_get_value_6() throws Exception {
        Assert.assertEquals(standard_text[40], bucketSort.log.get(40));
    }

    @Test
    public void test_of_bucket_6() throws Exception {
        Assert.assertEquals(standard_text[41], bucketSort.log.get(41));
    }

    @Test
    public void test_set_on_index_6() throws Exception {
        Assert.assertEquals(standard_text[42], bucketSort.log.get(42));
    }

    @Test
    public void test_get_value_7() throws Exception {
        Assert.assertEquals(standard_text[43], bucketSort.log.get(43));
    }

    @Test
    public void test_of_bucket_7() throws Exception {
        Assert.assertEquals(standard_text[44], bucketSort.log.get(44));
    }

    @Test
    public void test_set_on_index_7() throws Exception {
        Assert.assertEquals(standard_text[45], bucketSort.log.get(45));
    }

    @Test
    public void test_array_after_sort() throws Exception {
        Assert.assertEquals(standard_text[46], bucketSort.log.get(46));
    }
}