package org.labs.lab1.task3;

import org.junit.*;

import java.util.ArrayList;

/**
 * Created by Слава on 26.03.2017.
 */
public class TextTest {

    public Car car;
    public Hall hall;

    public Door door_forward;
    public Door door_another_end;

    public Light light;

    public Thing table1;
    public Thing table2;
    public Thing reqard1;
    public Thing reqard2;


    public Person person_art;
    public Person person_mag;

    @Before
    public void initTest() {
        ArrayList<Person> passengers = new ArrayList<Person>();
        car = new Car(passengers);

        person_art = new Person("Artur", car);
        person_mag = new Person("Old magratine", car);
        passengers.add(person_art);
        passengers.add(person_mag);

        hall = new Hall();
        table1 = new Thing(NAME.TABLE, MATERIAL.GLASS);
        table2 = new Thing(NAME.TABLE, MATERIAL.GLASS);
        reqard1 = new Thing(NAME.REGARD, MATERIAL.OF_TRANSPARENT_PLASTIC);
        reqard2 = new Thing(NAME.REGARD, MATERIAL.OF_TRANSPARENT_PLASTIC);
        Thing[] things = {table1, table2, reqard1, reqard2};

        door_forward = new Door(SIDE.FORWARD, hall);
        door_another_end = new Door(SIDE.ANOTHER_END, hall);
        Door[] doors = {door_forward, door_another_end};

        light = new Light(door_another_end, DIRECTION.OVER);
        hall.setDoors(doors);
        hall.setThings(things);
        hall.setLight(light);
    }

    @After
    public void afterTest() {
        car = null;
        hall = null;

        door_forward = null;
        door_another_end = null;

        light = null;

        table1 = null;
        table2 = null;
        reqard1 = null;
        reqard2 = null;

        person_art = null;
        person_mag = null;
    }

    public void do_car_move() {
        car.move(door_forward);
    }

    public void do_move_and_stop_car() {
        car.move(door_forward);
        car.stop(TRIP.NOT_LONG);
    }

    public void artur_get_out_of_the_car() {
        do_move_and_stop_car();
        person_art.getOutOfTheCar(car);
    }

    public void magratine_get_out_of_the_car() {
        do_move_and_stop_car();
        person_mag.getOutOfTheCar(car);
    }

    public void artur_and_magratine_get_out_of_the_car() {
        do_move_and_stop_car();
        person_art.getOutOfTheCar(car);
        person_mag.getOutOfTheCar(car);
    }

    public void artur_walk_through_the_door_in_hall() {
        artur_and_magratine_get_out_of_the_car();
        person_art.walkThroughTheDoor(door_forward);
    }

    public void magratine_walk_through_the_door_in_hall() {
        artur_and_magratine_get_out_of_the_car();
        person_mag.walkThroughTheDoor(door_forward);
    }

    public void artur_and_magratine_walk_through_the_door_in_hall() {
        artur_and_magratine_get_out_of_the_car();
        person_art.walkThroughTheDoor(door_forward);
        person_mag.walkThroughTheDoor(door_forward);
    }

    public void turn_on_light_in_hall() {
        artur_and_magratine_walk_through_the_door_in_hall();
        hall.light(TRIP.ALMOST_IMMEDIATELY, light);
    }

    public void artur_walk_through_the_door_out_hall() {
        turn_on_light_in_hall();
        person_art.walkThroughTheDoor(door_another_end);
    }

    public void magratine_walk_through_the_door_out_hall() {
        turn_on_light_in_hall();
        person_mag.walkThroughTheDoor(door_another_end);
    }

    public void artur_and_magratine_walk_through_the_door_out_hall() {
        turn_on_light_in_hall();
        person_art.walkThroughTheDoor(door_another_end);
        person_mag.walkThroughTheDoor(door_another_end);
    }
}
