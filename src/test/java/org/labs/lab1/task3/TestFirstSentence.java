package org.labs.lab1.task3;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by b1nt_ on 30.03.2017.
 *
 * После недолгой поездки на аэромобиле Артур и старый магратеянин оказались у какой-то двери.
 */
public class TestFirstSentence extends TextTest {

    // Testing objects before begin
    // Testing car
    @Test
    public void test_car_before_move_is_drive() throws Exception {
        Assert.assertEquals(false, car.isDrive());
    }

    @Test
    public void test_car_before_move_destination() throws Exception {
        Assert.assertEquals(null, car.getDestination());
    }
    
    @Test
    public void test_car_before_move_position() throws Exception {
        Assert.assertEquals(null, car.getPosition());
    }
    
    @Test
    public void test_car_before_move_passengers() throws Exception {
        Assert.assertTrue(!car.getPassengers().isEmpty());
    }
    
    // Testing Artur
    @Test
    public void test_artur_before_move_name() throws Exception {
        Assert.assertEquals("Artur", person_art.getName());
    }
    
    @Test
    public void test_artur_before_move_position() throws Exception {
        person_art.getName();
        Assert.assertEquals(car, person_art.getPosition());
    }
    
    @Test
    public void test_artur_before_move_through_doors() throws Exception {
        Assert.assertTrue(person_art.getThroughDoors().isEmpty());
    }
    
    // Testing Magratine
    @Test
    public void test_magratine_before_move_name() throws Exception {
        Assert.assertEquals("Old magratine", person_mag.getName());
    }
    
    @Test
    public void test_magratine_before_move_position() throws Exception {
        Assert.assertEquals(car, person_mag.getPosition());
    }
    
    @Test
    public void test_magratine_before_move_through_doors() throws Exception {
        Assert.assertTrue(person_mag.getThroughDoors().isEmpty());
    }

    // Testing door_forward
    @Test
    public void test_door_forward_before_move_location() throws Exception {
        Assert.assertEquals(hall, door_forward.getLocation());
    }

    @Test
    public void test_door_forward_before_move_side() throws Exception {
        Assert.assertEquals(SIDE.FORWARD, door_forward.getSide());
    }

    @Test
    public void test_door_forward_before_move_passed_people() throws Exception {
        Assert.assertTrue(door_forward.getPassedPeople().isEmpty());
    }
    // End testing objects before begin

    // Car is drive
    @Test
    public void test_car_move_is_drive() throws Exception {
        do_car_move();
        Assert.assertEquals(true, car.isDrive());
    }

    @Test
    public void test_car_move_direction() throws Exception {
        do_car_move();
        Assert.assertEquals(door_forward, car.getDestination());
    }

    @Test
    public void test_car_move_position() throws Exception {
        do_car_move();
        Assert.assertEquals(null, car.getPosition());
    }

    @Test
    public void test_car_move_passengers_size() throws Exception {
        do_car_move();
        Assert.assertTrue(car.getPassengers().size() == 2);
    }

    @Test
    public void test_car_move_passengers_artur() throws Exception {
        do_car_move();

        Person person_a = new Person("Artur", car);
        Assert.assertTrue(car.getPassengers().contains(person_a));
    }

    @Test
    public void test_car_move_passengers_magratine() throws Exception {
        do_car_move();

        Person person_m = new Person("Old magratine", car);
        Assert.assertTrue(car.getPassengers().contains(person_m));
    }

    // Car stop
    // Testing car
    @Test
    public void test_car_stop_is_drive() throws Exception {
        do_move_and_stop_car();
        Assert.assertEquals(false, car.isDrive());
    }

    @Test
    public void test_car_stop_destination() throws Exception {
        do_move_and_stop_car();
        Assert.assertEquals(null, car.getDestination());
    }

    @Test
    public void test_car_stop_position() throws Exception {
        do_move_and_stop_car();
        Assert.assertEquals(door_forward, car.getPosition());
    }

    @Test
    public void test_car_stop_passengers_size() throws Exception {
        do_move_and_stop_car();
        Assert.assertTrue(car.getPassengers().size() == 2);
    }

    @Test
    public void test_car_stop_passengers_artur() throws Exception {
        do_move_and_stop_car();

        Person person_a = new Person("Artur", car);
        Assert.assertTrue(car.getPassengers().contains(person_a));
    }

    @Test
    public void test_car_stop_passengers_magratine() throws Exception {
        do_move_and_stop_car();

        Person person_m = new Person("Old magratine", car);
        Assert.assertTrue(car.getPassengers().contains(person_m));
    }

    // Testing Artur
//    @Test
//    public void test_artur_after_car_stop_name() throws Exception {
//        do_move_and_stop_car();
//        Assert.assertEquals("Artur", person_art.getName());
//    }

    @Test
    public void test_artur_after_car_stop_position() throws Exception {
        do_move_and_stop_car();
        Assert.assertEquals(car, person_art.getPosition());
    }

    @Test
    public void test_artur_after_car_stop_through_doors() throws Exception {
        do_move_and_stop_car();
        Assert.assertTrue(person_art.getThroughDoors().isEmpty());
    }

    // Testing Magratine
    @Test
    public void test_magratine_after_car_stop_position() throws Exception {
        do_move_and_stop_car();
        Assert.assertEquals(car, person_mag.getPosition());
    }

    @Test
    public void test_magratine_after_car_stop_through_doors() throws Exception {
        do_move_and_stop_car();
        Assert.assertTrue(person_mag.getThroughDoors().isEmpty());
    }
}
