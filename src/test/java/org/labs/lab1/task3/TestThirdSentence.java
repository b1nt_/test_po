package org.labs.lab1.task3;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by b1nt_ on 30.03.2017.
 *
 * Почти сразу же над дверью в другом конце комнаты загорелся свет, и они вошли в нее.
 */
public class TestThirdSentence extends TextTest {

    // Testing light in hall
    @Test
    public void test_light_after_artur_and_magratine_in_hall_is_light() throws Exception {
        turn_on_light_in_hall();
        Assert.assertTrue(hall.getLight().isLight());
    }

    @Test
    public void test_light_after_artur_and_magratine_in_hall_direction() throws Exception {
        turn_on_light_in_hall();
        Assert.assertEquals(DIRECTION.OVER, hall.getLight().getDirection());
    }

    @Test
    public void test_light_after_artur_and_magratine_in_hall_door() throws Exception {
        turn_on_light_in_hall();
        Assert.assertEquals(door_another_end, hall.getLight().getBelong());
    }

    // Testing another_end_door after light turn on
    @Test
    public void test_another_end_door_after_artur_and_magratine_in_hall_and_light_turn_on_passed_people() throws Exception {
        turn_on_light_in_hall();
        Assert.assertTrue(door_another_end.getPassedPeople().isEmpty());
    }

    @Test
    public void test_another_end_door_after_artur_and_magratine_in_hall_and_light_turn_on_location() throws Exception {
        turn_on_light_in_hall();
        Assert.assertEquals(hall, door_another_end.getLocation());
    }

    @Test
    public void test_another_end_door_after_artur_and_magratine_in_hall_and_light_turn_on_side() throws Exception {
        turn_on_light_in_hall();
        Assert.assertEquals(SIDE.ANOTHER_END, door_another_end.getSide());
    }

    // Testing Artur through another door
    @Test
    public void test_artur_after_through_another_door_position() throws Exception {
        artur_walk_through_the_door_out_hall();
        Assert.assertEquals(null, person_art.getPosition());
    }

//    @Test
//    public void test_artur_after_through_another_door_name() throws Exception {
//        artur_walk_through_the_door_out_hall();
//        Assert.assertEquals("Artur", person_art.getName());
//    }

    @Test
    public void test_artur_after_through_another_door_through_doors_size() throws Exception {
        artur_walk_through_the_door_out_hall();
        Assert.assertTrue(person_art.getThroughDoors().size() == 2);
    }

    @Test
    public void test_artur_after_through_another_door_through_door_forward() throws Exception {
        artur_walk_through_the_door_out_hall();
        Assert.assertTrue(person_art.getThroughDoors().contains(door_forward));
    }

    @Test
    public void test_artur_after_through_another_door_through_door_another_end() throws Exception {
        artur_walk_through_the_door_out_hall();
        Assert.assertTrue(person_art.getThroughDoors().contains(door_another_end));
    }

    // Testing Magratine through another door
    @Test
    public void test_magratine_after_through_another_door_position() throws Exception {
        magratine_walk_through_the_door_out_hall();
        Assert.assertEquals(null, person_mag.getPosition());
    }

//    @Test
//    public void test_magratine_after_through_another_door_name() throws Exception {
//        magratine_walk_through_the_door_out_hall();
//        Assert.assertEquals("Old magratine", person_mag.getName());
//    }

    @Test
    public void test_magratine_after_through_another_door_through_doors_size() throws Exception {
        magratine_walk_through_the_door_out_hall();
        Assert.assertTrue(person_mag.getThroughDoors().size() == 2);
    }

    @Test
    public void test_artur_magratine_through_another_door_through_door_forward() throws Exception {
        magratine_walk_through_the_door_out_hall();
        Assert.assertTrue(person_mag.getThroughDoors().contains(door_forward));
    }

    @Test
    public void test_artur_magratine_through_another_door_through_door_another_end() throws Exception {
        magratine_walk_through_the_door_out_hall();
        Assert.assertTrue(person_mag.getThroughDoors().contains(door_another_end));
    }

    // Testing another end door after Artur through
    @Test
    public void test_another_end_door_after_artur_through_passed_people_size() throws Exception{
        artur_walk_through_the_door_out_hall();
        Assert.assertTrue(door_another_end.getPassedPeople().size() == 1);
    }

    @Test
    public void test_another_end_door_after_artur_through_passed_people_artur() throws Exception{
        artur_walk_through_the_door_out_hall();
        Assert.assertTrue(door_another_end.getPassedPeople().contains(person_art));
    }

    // Testing another end door after Magratine through
    @Test
    public void test_another_end_door_after_magratine_through_passed_people_size() throws Exception{
        magratine_walk_through_the_door_out_hall();
        Assert.assertTrue(door_another_end.getPassedPeople().size() == 1);
    }

    @Test
    public void test_another_end_door_after_magratine_through_passed_people_magratine() throws Exception{
        magratine_walk_through_the_door_out_hall();
        Assert.assertTrue(door_another_end.getPassedPeople().contains(person_mag));
    }

    // Testing another end dorr after Artur and Magratine through
    @Test
    public void test_another_end_door_after_artur_and_magratine_through_passed_people_size() throws Exception{
        artur_and_magratine_walk_through_the_door_out_hall();
        Assert.assertTrue(door_another_end.getPassedPeople().size() == 2);
    }

    @Test
    public void test_another_end_door_after_artur_and_magratine_through_passed_people_artur() throws Exception{
        artur_and_magratine_walk_through_the_door_out_hall();
        Assert.assertTrue(door_another_end.getPassedPeople().contains(person_art));
    }

    @Test
    public void test_another_end_door_after_artur_and_magratine_through_passed_people_magratine() throws Exception{
        artur_and_magratine_walk_through_the_door_out_hall();
        Assert.assertTrue(door_another_end.getPassedPeople().contains(person_mag));
    }

    // Testing hall after Artur and Magratine through another end door
    @Test
    public void test_hall_after_artur_and_magratine_through_another_end_door_guests() throws Exception {
        artur_and_magratine_walk_through_the_door_out_hall();
        Assert.assertTrue(hall.getGuests().isEmpty());
    }

    @Test
    public void test_hall_after_artur_and_magratine_through_another_end_door_light() throws Exception {
        artur_and_magratine_walk_through_the_door_out_hall();
        Assert.assertTrue(hall.getLight().isLight());
    }
}
