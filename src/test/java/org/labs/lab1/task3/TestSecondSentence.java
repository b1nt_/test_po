package org.labs.lab1.task3;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by b1nt_ on 30.03.2017.
 *
 * Они вышли из машины и прошли в приемную,
 * уставленную стеклянными столиками и наградами из прозрачного пластика.
 */
public class TestSecondSentence extends TextTest {

    // Artur and Magratine get out of the car
    // Testing Artur
    @Test
    public void test_artur_before_go_to_hall_position() throws Exception {
        artur_get_out_of_the_car();
        Assert.assertEquals(door_forward, person_art.getPosition());
    }

//    @Test
//    public void test_artur_before_go_to_hall_name() throws Exception {
//        artur_get_out_of_the_car();
//        Assert.assertEquals("Artur", person_art.getName());
//    }

    @Test
    public void test_artur_before_go_to_hall_through_doors_count() throws Exception {
        artur_get_out_of_the_car();
        Assert.assertTrue(person_art.getThroughDoors().isEmpty());
    }

    // Testing Magratine
    @Test
    public void test_magratine_before_go_to_hall_position() throws Exception {
        magratine_get_out_of_the_car();
        Assert.assertEquals(door_forward, person_mag.getPosition());
    }

//    @Test
//    public void test_magratine_before_go_to_hall_name() throws Exception {
//        magratine_get_out_of_the_car();
//        Assert.assertEquals("Old magratine", person_mag.getName());
//    }

    @Test
    public void test_magratine_before_go_to_hall_through_doors_count() throws Exception {
        magratine_get_out_of_the_car();
        Assert.assertTrue(person_mag.getThroughDoors().isEmpty());
    }

    // Testing car
    @Test
    public void test_car_after_stop_and_get_out_passengers() throws Exception {
        artur_and_magratine_get_out_of_the_car();
        Assert.assertTrue(car.getPassengers().isEmpty());
    }

    @Test
    public void test_car_after_stop_and_get_out_position() throws Exception {
        artur_and_magratine_get_out_of_the_car();
        Assert.assertEquals(door_forward, car.getPosition());
    }

    @Test
    public void test_car_after_stop_and_get_out_destination() throws Exception {
        artur_and_magratine_get_out_of_the_car();
        Assert.assertEquals(null, car.getDestination());
    }

    // Artur and Magratine through door_forward
    // Testing Artur
    @Test
    public void test_artur_after_through_the_first_door_position() throws Exception {
        artur_walk_through_the_door_in_hall();
        Assert.assertEquals(hall, person_art.getPosition());
    }

//    @Test
//    public void test_artur_after_through_the_first_door_name() throws Exception {
//        artur_walk_through_the_door_in_hall();
//        Assert.assertEquals("Artur", person_art.getName());
//    }

    @Test
    public void test_artur_after_through_the_first_door_through_doors_count() throws Exception {
        artur_walk_through_the_door_in_hall();
        Assert.assertTrue(person_art.getThroughDoors().size() == 1);
    }

    @Test
    public void test_artur_after_through_the_first_door_through_doors() throws Exception {
        artur_walk_through_the_door_in_hall();
        Assert.assertTrue(person_art.getThroughDoors().contains(door_forward));
    }

    // Testing Magratine
    @Test
    public void test_magratine_position_after_through_the_first_door() throws Exception {
        magratine_walk_through_the_door_in_hall();
        Assert.assertEquals(hall, person_mag.getPosition());
    }

//    @Test
//    public void test_magratine_after_through_the_first_door_name() throws Exception {
//        magratine_walk_through_the_door_in_hall();
//        Assert.assertEquals("Old magratine", person_mag.getName());
//    }

    @Test
    public void test_magratine_after_through_the_first_door_through_doors_count() throws Exception {
        magratine_walk_through_the_door_in_hall();
        Assert.assertTrue(person_mag.getThroughDoors().size() == 1);
    }

    @Test
    public void test_magratine_after_through_the_first_door_through_doors() throws Exception {
        magratine_walk_through_the_door_in_hall();
        Assert.assertTrue(person_mag.getThroughDoors().contains(door_forward));
    }

    // Testing door_forward
    // After Artur through
    @Test
    public void test_door_forward_after_artur_through_location() throws Exception {
        artur_walk_through_the_door_in_hall();
        Assert.assertEquals(hall, door_forward.getLocation());
    }

    @Test
    public void test_door_forward_after_artur_through_side() throws Exception {
        artur_walk_through_the_door_in_hall();
        Assert.assertEquals(SIDE.FORWARD, door_forward.getSide());
    }

    @Test
    public void test_door_forward_after_artur_through_passed_people_size() throws Exception {
        artur_walk_through_the_door_in_hall();
        Assert.assertTrue(door_forward.getPassedPeople().size() == 1);
    }

    @Test
    public void test_door_forward_after_artur_through_passed_people() throws Exception {
        artur_walk_through_the_door_in_hall();
        Assert.assertTrue(door_forward.getPassedPeople().contains(person_art));
    }
    
    // After Magratine through
//    @Test
//    public void test_door_forward_after_magratine_through_location() throws Exception {
//        magratine_walk_through_the_door_in_hall();
//        Assert.assertEquals(hall, door_forward.getLocation());
//    }
//
//    @Test
//    public void test_door_forward_after_magratine_through_side() throws Exception {
//        magratine_walk_through_the_door_in_hall();
//        Assert.assertEquals(SIDE.FORWARD, door_forward.getSide());
//    }

    @Test
    public void test_door_forward_after_magratine_through_passed_people_size() throws Exception {
        magratine_walk_through_the_door_in_hall();
        Assert.assertTrue(door_forward.getPassedPeople().size() == 1);
    }

    @Test
    public void test_door_forward_after_magratine_through_passed_people() throws Exception {
        magratine_walk_through_the_door_in_hall();
        Assert.assertTrue(door_forward.getPassedPeople().contains(person_mag));
    }
    
    // After Artur and Magratine through
    @Test
    public void test_door_forward_after_artur_and_magratine_through_passed_people_size() throws Exception {
        artur_and_magratine_walk_through_the_door_in_hall();
        Assert.assertTrue(door_forward.getPassedPeople().size() == 2);
    }

    @Test
    public void test_door_forward_after_artur_and_magratine_through_passed_people_artur() throws Exception {
        artur_and_magratine_walk_through_the_door_in_hall();
        Assert.assertTrue(door_forward.getPassedPeople().contains(person_art));
    }

    @Test
    public void test_door_forward_after_artur_and_magratine_through_passed_people_magratine() throws Exception {
        artur_and_magratine_walk_through_the_door_in_hall();
        Assert.assertTrue(door_forward.getPassedPeople().contains(person_mag));
    }

//    @Test
//    public void test_door_forward_after_artur_and_magratine_through_side() throws Exception {
//        artur_and_magratine_walk_through_the_door_in_hall();
//        Assert.assertEquals(SIDE.FORWARD, door_forward.getSide());
//    }
//
//    @Test
//    public void test_door_forward_after_artur_and_magratine_through_location() throws Exception {
//        artur_and_magratine_walk_through_the_door_in_hall();
//        Assert.assertEquals(hall, door_forward.getLocation());
//    }
    // End testing door

    // Testing hall
    @Test
    public void test_hall_doors_size() throws Exception{
        Assert.assertTrue(hall.getDoors().length == 2);
    }

    @Test
    public void test_hall_doors_door_forward() throws Exception{
        int door_forward_in_hall = 0;
        for(Door door : hall.getDoors()) {
            if(door.equals(door_forward)) {
                door_forward_in_hall++;
            }
        }
        Assert.assertTrue(door_forward_in_hall == 1);
    }

    @Test
    public void test_hall_doors_door_another_end() throws Exception{
        int door_another_end_in_hall = 0;
        for(Door door : hall.getDoors()) {
            if(door.equals(door_forward)) {
                door_another_end_in_hall++;
            }
        }
        Assert.assertTrue(door_another_end_in_hall == 1);
    }

    // Testing Artur in hall
    @Test
    public void test_hall_after_artur_through_forward_door_guests() throws Exception {
        artur_walk_through_the_door_in_hall();
        Assert.assertTrue(hall.getGuests().contains(person_art));
    }

    @Test
    public void test_hall_after_artur_through_forward_door_guests_size() throws Exception {
        artur_walk_through_the_door_in_hall();
        Assert.assertTrue(hall.getGuests().size() == 1);
    }

    @Test
    public void test_hall_after_artur_through_forward_door_light() throws Exception {
        artur_walk_through_the_door_in_hall();
        Assert.assertFalse(hall.getLight().isLight());
    }

    // Testing Magratine in hall
    @Test
    public void test_hall_after_magratine_through_forward_door_guests() throws Exception {
        magratine_walk_through_the_door_in_hall();
        Assert.assertTrue(hall.getGuests().contains(person_mag));
    }

    @Test
    public void test_hall_after_magratine_through_forward_door_guests_size() throws Exception {
        magratine_walk_through_the_door_in_hall();
        Assert.assertTrue(hall.getGuests().size() == 1);
    }

    @Test
    public void test_hall_after_magratine_through_forward_door_light() throws Exception {
        magratine_walk_through_the_door_in_hall();
        Assert.assertFalse(hall.getLight().isLight());
    }

    // Testing Artur and Magratine in hall
    @Test
    public void test_hall_after_artur_and_magratine_through_forward_door_guests_size() throws Exception {
        artur_and_magratine_walk_through_the_door_in_hall();
        Assert.assertTrue(hall.getGuests().size() == 2);
    }

    @Test
    public void test_hall_after_artur_and_magratine_through_forward_door_guest_artur() throws Exception {
        artur_and_magratine_walk_through_the_door_in_hall();
        Assert.assertTrue(hall.getGuests().contains(person_art));
    }

    @Test
    public void test_hall_after_artur_and_magratine_through_forward_door_guest_magratine() throws Exception {
        artur_and_magratine_walk_through_the_door_in_hall();
        Assert.assertTrue(hall.getGuests().contains(person_mag));
    }

    @Test
    public void test_hall_after_artur_and_magratine_through_forward_door_light() throws Exception {
        artur_and_magratine_walk_through_the_door_in_hall();
        Assert.assertFalse(hall.getLight().isLight());
    }

    // Testing things
    @Test
    public void test_hall_with_things() throws Exception{
        artur_and_magratine_walk_through_the_door_in_hall();
        Assert.assertTrue(hall.getThings().length > 3);
    }

    @Test
    public void test_hall_with_table() throws Exception {
        artur_and_magratine_walk_through_the_door_in_hall();

        int table_count = 0;
        for(Thing thing : hall.getThings()) {
            if(NAME.TABLE.equals(thing.getName()) && MATERIAL.GLASS.equals(thing.getMaterial())) {
                table_count++;
            }
        }
        Assert.assertTrue(table_count > 1);
    }

    @Test
    public void test_hall_with_regards() throws Exception {
        artur_and_magratine_walk_through_the_door_in_hall();

        int regard_count = 0;
        for(Thing thing : hall.getThings()) {
            if(NAME.REGARD.equals(thing.getName()) && MATERIAL.OF_TRANSPARENT_PLASTIC.equals(thing.getMaterial())) {
                regard_count++;
            }
        }
        Assert.assertTrue(regard_count > 1);
    }

    @Test
    public void test_hall_with_table_and_ragards_only() throws Exception{
        artur_and_magratine_walk_through_the_door_in_hall();

        int table_and_regard_count = 0;
        for(Thing thing : hall.getThings()) {
            if(!(NAME.REGARD.equals(thing.getName()) || NAME.TABLE.equals(thing.getName())) ||
                    !(MATERIAL.OF_TRANSPARENT_PLASTIC.equals(thing.getMaterial()) || MATERIAL.GLASS.equals(thing.getMaterial()))) {
                table_and_regard_count++;
            }
        }
        Assert.assertTrue(table_and_regard_count == 0);
    }
}
