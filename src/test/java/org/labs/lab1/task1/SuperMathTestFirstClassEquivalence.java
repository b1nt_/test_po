package org.labs.lab1.task1;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Random;

/**
 * Created by Слава on 26.03.2017.
 */
public class SuperMathTestFirstClassEquivalence {

    private SuperMath superMath;
    private Random random;

    private final double e = 0.0001;
    private final int max_degree = 79;

    private double angle;

    @Before
    public void initTest() {
        superMath = new SuperMath();
        random = new Random(System.currentTimeMillis());
        angle = Math.toRadians((double) random.nextInt(max_degree));
    }

    @After
    public void afterTest() {
        superMath = null;
        random = null;
    }

    // Testing 0 < angle < 90 degrees value
    @Test
    public void test_tg_for_value_from_0_ut_90() throws Exception {
        Assert.assertEquals("Angle is " + Math.toDegrees(angle), Math.tan(angle), superMath.tg(angle), e);
    }

    // Testing 0 < angle < 90 degrees value + PI
    @Test
    public void test_tg_for_value_from_0_ut_90_plus_PI() throws Exception {
        Assert.assertEquals("Angle is " + Math.toDegrees(angle) + " + PI", Math.tan(angle + Math.PI), superMath.tg(angle + Math.PI), e);
    }

    // Testing 0 < angle < 90 degrees value + 2 * PI
    @Test
    public void test_tg_for_value_from_0_ut_90_plus_2PI() throws Exception {
        Assert.assertEquals("Angle is " + Math.toDegrees(angle) + " + 2 * PI", Math.tan(angle + 2 * Math.PI), superMath.tg(angle + 2 * Math.PI), e);
    }

    // Testing 0 < angle < 90 degrees value + 12 * PI
    @Test
    public void test_tg_for_value_from_0_ut_90_plus_12PI() throws Exception {
        Assert.assertEquals("Angle is " + Math.toDegrees(angle) + " + 12 * PI", Math.tan(angle + 12 * Math.PI), superMath.tg(angle + 12 * Math.PI), e);
    }

    // Testing 0 < angle < 90 degrees value + 112 * PI
    @Test
    public void test_tg_for_value_from_0_ut_90_plus_112PI() throws Exception {
        Assert.assertEquals("Angle is " + Math.toDegrees(angle) + " + 112 * PI", Math.tan(angle + 112 * Math.PI), superMath.tg(angle + 112 * Math.PI), e);
    }

    // Testing 0 < angle < 90 degrees value - PI
    @Test
    public void test_tg_for_value_from_minus_90_ut_0_minus_PI() throws Exception {
        Assert.assertEquals("Angle is " + Math.toDegrees(angle) + " - PI", Math.tan(angle - Math.PI), superMath.tg(angle - Math.PI), e);
    }

    // Testing 0 < angle < 90 degrees value - 2 * PI
    @Test
    public void test_tg_for_value_from_minus_90_ut_0_minus_2PI() throws Exception {
        Assert.assertEquals("Angle is " + Math.toDegrees(angle) + " - 2 * PI", Math.tan(angle - 2 * Math.PI), superMath.tg(angle - 2 * Math.PI), e);
    }

    // Testing 0 < angle < 90 degrees value - 2 * PI
    @Test
    public void test_tg_for_value_from_minus_90_ut_0_minus_12PI() throws Exception {
        Assert.assertEquals("Angle is " + Math.toDegrees(angle) + " - 12 * PI", Math.tan(angle - 12 * Math.PI), superMath.tg(angle - 12 * Math.PI), e);
    }

    // Testing 0 < angle < 90 degrees value - 2 * PI
    @Test
    public void test_tg_for_value_from_minus_90_ut_0_minus_112PI() throws Exception {
        Assert.assertEquals("Angle is " + Math.toDegrees(angle) + " - 112 * PI", Math.tan(angle - 112 * Math.PI), superMath.tg(angle - 112 * Math.PI), e);
    }
}
