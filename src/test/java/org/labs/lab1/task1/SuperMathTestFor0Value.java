package org.labs.lab1.task1;

import org.junit.*;

/**
 * Created by Слава on 11.03.2017.
 */
public class SuperMathTestFor0Value {

    private SuperMath superMath;

    private final double e = 0.0001;
    private final double d = 0.00001;

    @Before
    public void initTest() {
        superMath = new SuperMath();
    }

    @After
    public void afterTest() {
        superMath = null;
    }

    @Test
    public void test_tg_for_zero_value() throws Exception {
        Assert.assertEquals(Math.tan(0), superMath.tg(0), e);
    }

    @Test
    public void test_tg_for_zero_value_plus_delta() throws Exception {
        Assert.assertEquals(Math.tan(0), superMath.tg(0 + d), e);
    }

    @Test
    public void test_tg_for_zero_value_minus_delta() throws Exception {
        Assert.assertEquals(Math.tan(0), superMath.tg(0 - d), e);
    }

    // 0 + PI
    @Test
    public void test_tg_for_zero_value_plus_PI() throws Exception {
        Assert.assertEquals(Math.tan(0 + Math.PI), superMath.tg(0 + Math.PI), e);
    }

    @Test
    public void test_tg_for_zero_value_plus_PI_plus_delta() throws Exception {
        Assert.assertEquals(Math.tan(0 + Math.PI), superMath.tg(0 + d + Math.PI), e);
    }

    @Test
    public void test_tg_for_zero_value_plus_PI_minus_delta() throws Exception {
        Assert.assertEquals(Math.tan(0 + Math.PI), superMath.tg(0 - d + Math.PI), e);
    }

    // 0 + 2 * PI
    @Test
    public void test_tg_for_zero_value_plus_2PI() throws Exception {
        Assert.assertEquals(Math.tan(0 + 2 * Math.PI), superMath.tg(0 + 2 * Math.PI), e);
    }

    @Test
    public void test_tg_for_zero_value_plus_2PI_plus_delta() throws Exception {
        Assert.assertEquals(Math.tan(0 + 2 * Math.PI), superMath.tg(0 + d + 2 * Math.PI), e);
    }

    @Test
    public void test_tg_for_zero_value_plus_2PI_minus_delta() throws Exception {
        Assert.assertEquals(Math.tan(0 + 2 * Math.PI), superMath.tg(0 - d + 2 * Math.PI), e);
    }

    // 0 + 12 * PI
    @Test
    public void test_tg_for_zero_value_plus_12PI() throws Exception {
        Assert.assertEquals(Math.tan(0 + 12 * Math.PI), superMath.tg(0 + 12 * Math.PI), e);
    }

    @Test
    public void test_tg_for_zero_value_plus_12PI_plus_delta() throws Exception {
        Assert.assertEquals(Math.tan(0 + 12 * Math.PI), superMath.tg(0 + d + 12 * Math.PI), e);
    }

    @Test
    public void test_tg_for_zero_value_plus_12PI_minus_delta() throws Exception {
        Assert.assertEquals(Math.tan(0 + 12 * Math.PI), superMath.tg(0 - d + 12 * Math.PI), e);
    }

    // 0 + 112 * PI
    @Test
    public void test_tg_for_zero_value_plus_112PI() throws Exception {
        Assert.assertEquals(Math.tan(0 + 112 * Math.PI), superMath.tg(0 + 112 * Math.PI), e);
    }

    @Test
    public void test_tg_for_zero_value_plus_112PI_plus_delta() throws Exception {
        Assert.assertEquals(Math.tan(0 + 112 * Math.PI), superMath.tg(0 + d + 112 * Math.PI), e);
    }

    @Test
    public void test_tg_for_zero_value_plus_112PI_minus_delta() throws Exception {
        Assert.assertEquals(Math.tan(0 + 112 * Math.PI), superMath.tg(0 - d + 112 * Math.PI), e);
    }


    // 0 - PI
    @Test
    public void test_tg_for_zero_value_minus_PI() throws Exception {
        Assert.assertEquals(Math.tan(0 - Math.PI), superMath.tg(0 - Math.PI), e);
    }

    @Test
    public void test_tg_for_zero_value_minus_PI_plus_delta() throws Exception {
        Assert.assertEquals(Math.tan(0 - Math.PI), superMath.tg(0 + d - Math.PI), e);
    }

    @Test
    public void test_tg_for_zero_value_minus_PI_minus_delta() throws Exception {
        Assert.assertEquals(Math.tan(0 - Math.PI), superMath.tg(0 - d - Math.PI), e);
    }

    // 0 - 2 * PI
    @Test
    public void test_tg_for_zero_value_minus_2PI() throws Exception {
        Assert.assertEquals(Math.tan(0 - 2 * Math.PI), superMath.tg(0 - 2 * Math.PI), e);
    }

    @Test
    public void test_tg_for_zero_value_minus_2PI_plus_delta() throws Exception {
        Assert.assertEquals(Math.tan(0 - 2 * Math.PI), superMath.tg(0 + d - 2 * Math.PI), e);
    }

    @Test
    public void test_tg_for_zero_value_minus_2PI_minus_delta() throws Exception {
        Assert.assertEquals(Math.tan(0 - 2 * Math.PI), superMath.tg(0 - d - 2 * Math.PI), e);
    }

    // 0 - 12 * PI
    @Test
    public void test_tg_for_zero_value_minus_12PI() throws Exception {
        Assert.assertEquals(Math.tan(0 - 12 * Math.PI), superMath.tg(0 - 12 * Math.PI), e);
    }

    @Test
    public void test_tg_for_zero_value_minus_12PI_plus_delta() throws Exception {
        Assert.assertEquals(Math.tan(0 - 12 * Math.PI), superMath.tg(0 + d - 12 * Math.PI), e);
    }

    @Test
    public void test_tg_for_zero_value_minus_12PI_minus_delta() throws Exception {
        Assert.assertEquals(Math.tan(0 - 12 * Math.PI), superMath.tg(0 - d - 12 * Math.PI), e);
    }

    // 0 - 112 * PI
    @Test
    public void test_tg_for_zero_value_minus_112PI() throws Exception {
        Assert.assertEquals(Math.tan(0 - 112 * Math.PI), superMath.tg(0 - 112 * Math.PI), e);
    }

    @Test
    public void test_tg_for_zero_value_minus_112PI_plus_delta() throws Exception {
        Assert.assertEquals(Math.tan(0 - 112 * Math.PI), superMath.tg(0 + d - 112 * Math.PI), e);
    }

    @Test
    public void test_tg_for_zero_value_minus_112PI_minus_delta() throws Exception {
        Assert.assertEquals(Math.tan(0 - 112 * Math.PI), superMath.tg(0 - d - 112 * Math.PI), e);
    }
}