package org.labs.lab1.task1;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Слава on 26.03.2017.
 */
public class SuperMathTestFor90Value {

    private SuperMath superMath;

    private final double e = 0.0001;
    private final double d = 0.00001;
    private final double angle = Math.PI / 2;

    @Before
    public void initTest() {
        superMath = new SuperMath();
    }

    @After
    public void afterTest() {
        superMath = null;
    }

    // Testing 90 degrees value
    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_90_degrees() throws Exception {
        Assert.assertEquals(Math.tan(angle), superMath.tg(angle), e);
    }

    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_90_degrees_plus_delta() throws Exception {
        Assert.assertEquals(Math.tan(angle), superMath.tg(angle + d), e);
    }

    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_90_degrees_minus_delta() throws Exception {
        Assert.assertEquals(Math.tan(angle), superMath.tg(angle - d), e);
    }

    // Testing 90 degrees + PI
    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_90_degrees_plus_PI() throws Exception {
        Assert.assertEquals(Math.tan(angle + Math.PI), superMath.tg(angle + Math.PI), e);
    }

    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_90_degrees_plus_PI_plus_delta() throws Exception {
        Assert.assertEquals(Math.tan(angle + Math.PI), superMath.tg(angle + d + Math.PI), e);
    }

    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_90_degrees_plus_PI_minus_delta() throws Exception {
        Assert.assertEquals(Math.tan(angle + Math.PI), superMath.tg(angle - d + Math.PI), e);
    }

    // Testing 90 degrees + 2 * PI
    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_90_degrees_plus_2PI() throws Exception {
        Assert.assertEquals(Math.tan(angle + 2 * Math.PI), superMath.tg(angle + 2 * Math.PI), e);
    }

    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_90_degrees_plus_2PI_plus_delta() throws Exception {
        Assert.assertEquals(Math.tan(angle + 2 * Math.PI), superMath.tg(angle + d + 2 * Math.PI), e);
    }

    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_90_degrees_plus_2PI_minus_delta() throws Exception {
        Assert.assertEquals(Math.tan(angle + 2 * Math.PI), superMath.tg(angle - d + 2 * Math.PI), e);
    }

    // Testing 90 degrees + 12 * PI
    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_90_degrees_plus_12PI() throws Exception {
        Assert.assertEquals(Math.tan(angle + 12 * Math.PI), superMath.tg(angle + 12 * Math.PI), e);
    }

    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_90_degrees_plus_12PI_plus_delta() throws Exception {
        Assert.assertEquals(Math.tan(angle + 12 * Math.PI), superMath.tg(angle + d + 12 * Math.PI), e);
    }

    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_90_degrees_plus_12PI_minus_delta() throws Exception {
        Assert.assertEquals(Math.tan(angle + 12 * Math.PI), superMath.tg(angle - d + 12 * Math.PI), e);
    }

    // Testing 90 degrees + 112 * PI
    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_90_degrees_plus_112PI() throws Exception {
        Assert.assertEquals(Math.tan(angle + 112 * Math.PI), superMath.tg(angle + 112 * Math.PI), e);
    }

    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_90_degrees_plus_112PI_plus_delta() throws Exception {
        Assert.assertEquals(Math.tan(angle + 112 * Math.PI), superMath.tg(angle + d + 112 * Math.PI), e);
    }

    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_90_degrees_plus_112PI_minus_delta() throws Exception {
        Assert.assertEquals(Math.tan(angle + 112 * Math.PI), superMath.tg(angle - d + 112 * Math.PI), e);
    }

    // Testing -90 degrees value
    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_minus_90_degrees() throws Exception {
        Assert.assertEquals(Math.tan(-angle), superMath.tg(-angle), e);
    }

    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_minus_90_degrees_plus_delta() throws Exception {
        Assert.assertEquals(Math.tan(-angle), superMath.tg(-angle + d), e);
    }

    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_minus_90_degrees_minus_delta() throws Exception {
        Assert.assertEquals(Math.tan(-angle), superMath.tg(-angle - d), e);
    }

    // Testing -90 degrees - PI
    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_minus_90_degrees_plus_pi() throws Exception {
        Assert.assertEquals(Math.tan(-angle - Math.PI), superMath.tg(-angle - Math.PI), e);
    }

    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_minus_90_degrees_plus_PI_plus_delta() throws Exception {
        Assert.assertEquals(Math.tan(-angle - Math.PI), superMath.tg(-angle + d - Math.PI), e);
    }

    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_minus_90_degrees_plus_PI_minus_delta() throws Exception {
        Assert.assertEquals(Math.tan(-angle - Math.PI), superMath.tg(-angle - d - Math.PI), e);
    }

    // Testing -90 degrees - 2 * PI
    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_minus_90_degrees_plus_2PI() throws Exception {
        Assert.assertEquals(Math.tan(-angle - 2 * Math.PI), superMath.tg(-angle - 2 * Math.PI), e);
    }

    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_minus_90_degrees_plus_2PI_plus_delta() throws Exception {
        Assert.assertEquals(Math.tan(-angle - 2 * Math.PI), superMath.tg(-angle + d - 2 * Math.PI), e);
    }

    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_minus_90_degrees_plus_2PI_minus_delta() throws Exception {
        Assert.assertEquals(Math.tan(-angle - 2 * Math.PI), superMath.tg(-angle - d - 2 * Math.PI), e);
    }

    // Testing -90 degrees - 12 * PI
    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_minus_90_degrees_plus_12PI() throws Exception {
        Assert.assertEquals(Math.tan(-angle - 12 * Math.PI), superMath.tg(-angle - 12 * Math.PI), e);
    }

    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_minus_90_degrees_plus_12PI_plus_delta() throws Exception {
        Assert.assertEquals(Math.tan(-angle - 12 * Math.PI), superMath.tg(-angle + d - 12 * Math.PI), e);
    }

    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_minus_90_degrees_plus_12PI_minus_delta() throws Exception {
        Assert.assertEquals(Math.tan(-angle - 12 * Math.PI), superMath.tg(-angle - d - 12 * Math.PI), e);
    }

    // Testing -90 degrees - 112 * PI
    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_minus_90_degrees_plus_112PI() throws Exception {
        Assert.assertEquals(Math.tan(-angle - 112 * Math.PI), superMath.tg(-angle - 112 * Math.PI), e);
    }

    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_minus_90_degrees_plus_112PI_plus_delta() throws Exception {
        Assert.assertEquals(Math.tan(-angle - 112 * Math.PI), superMath.tg(-angle + d - 112 * Math.PI), e);
    }

    @Test(expected = java.lang.AssertionError.class)
    public void test_tg_value_minus_90_degrees_plus_112PI_minus_delta() throws Exception {
        Assert.assertEquals(Math.tan(-angle - 112 * Math.PI), superMath.tg(-angle - d - 112 * Math.PI), e);
    }
}
