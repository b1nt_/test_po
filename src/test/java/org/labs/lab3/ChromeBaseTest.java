package org.labs.lab3;

import org.labs.lab3.utils.Profile;

/**
 * Created by Слава on 09.05.2017.
 */
public class ChromeBaseTest extends BaseTest {
    private final String nameBrowser = "chrome";
    private final String baseUrl = "http://goldenbirds.biz/";

    protected final Profile profile = new Profile(myIO);

    public void initWithoutLogin() {
        init(nameBrowser, baseUrl);
    }

    public void initAndLogin() {
        init(nameBrowser, baseUrl, profile);
    }
}