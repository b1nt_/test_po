package org.labs.lab3.firefox;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.labs.lab3.GeckoBaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static org.junit.Assert.assertEquals;

/**
 * Created by Слава on 24.04.2017.
 */
public class TestPayChicken extends GeckoBaseTest{

    @Before
    public void setUp() throws Exception {
        initAndLogin();
    }

    @Test
    public void test1() throws Exception {
        driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/div[2]/header/ul/li[6]/a")));
        driver.findElement(By.xpath(".//*[@id='birds2_menu']/li[1]/a")).click();
        driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div[2]/div[5]/input[1]")));
        driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div[2]/div[5]/input[1]")).click();

        driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div[2]/div[5]/input[1]")));
        driver.findElement(By.xpath(".//*[@id='profile_menu']/li[1]/a")).click();

        driverWait.until(ExpectedConditions.urlToBe("http://goldenbirds.biz/profile"));
        try {
            assertEquals("0.00" , driver.findElement(By.xpath(".//*[@id='currency_menu']/li[1]/span")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @After
    public void tearDown() throws Exception {
        after();
    }
}
