package org.labs.lab3.firefox;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.labs.lab3.GeckoBaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static org.junit.Assert.assertEquals;

/**
 * Created by Слава on 23.04.2017.
 */
public class TestGoToVideoPage extends GeckoBaseTest{

    @Before
    public void setUp() throws Exception {
        initWithoutLogin();
    }

    @Test
    public void test1() throws Exception {
        driver.findElement(By.xpath("html/body/div[2]/header/a[1]")).click();

        driverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("html/body/div[2]/div/div[2]/div[1]")));
        try {
            assertEquals("Видео / goldenbirds.biz", driver.getTitle());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @After
    public void tearDown() throws Exception {
        after();
    }
}
