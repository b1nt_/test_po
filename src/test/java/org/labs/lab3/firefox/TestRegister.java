package org.labs.lab3.firefox;

import org.labs.lab3.GeckoBaseTest;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.*;

/**
 * Created by Слава on 23.04.2017.
 */
public class TestRegister extends GeckoBaseTest {


    @Before
    public void setUp() throws Exception {
        initWithoutLogin();
    }

    @Test
    public void test1() throws Exception {
        JavascriptExecutor jse = (JavascriptExecutor)driver;

        driver.findElement(By.xpath("html/body/div[2]/header/div[1]/div/div[3]/a/span")).click();

        WebElement email = driver.findElement(By.xpath(".//*[@id='reg_form']/input[1]"));
        String str_email = "test" + Math.round(Math.random()*111111) + "@mail.ru";
        jse.executeScript("arguments[0].value='" + str_email + "';", email);

        driver.findElement(By.xpath(".//*[@id='recaptcha-reg']")).click();

        driverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='reg_form']/table/tbody/tr[1]/td[2]/input")));

        WebElement login = driver.findElement(By.xpath(".//*[@id='reg_form']/table/tbody/tr[1]/td[2]/input"));
        String str_login = "test" + Math.round(Math.random()*111111);
        jse.executeScript("arguments[0].value='" + str_login + "';", login);

        WebElement pass = driver.findElement(By.xpath(".//*[@id='reg_form']/table/tbody/tr[2]/td[2]/input"));
        String str_password = "1234567";
        jse.executeScript("arguments[0].value='" + str_password + "';", pass);

        WebElement passeg = driver.findElement(By.xpath(".//*[@id='reg_form']/table/tbody/tr[3]/td[2]/input"));
        jse.executeScript("arguments[0].value='" + str_password + "';", passeg);

        driver.findElement(By.xpath(".//*[@id='reg_form']/input[1]")).click();
        driver.findElement(By.xpath(".//*[@id='reg_form']/input[2]")).submit();

        driverWait.until(ExpectedConditions.urlToBe("http://goldenbirds.biz/profile"));
        driver.findElement(By.xpath(".//*[@id='step-0']/div[3]/button")).click();
        try {
            assertEquals("Профиль / goldenbirds.biz", driver.getTitle());
            myIO.write(str_email, str_password);
            System.err.println(str_email + "\t" + str_password);
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @After
    public void tearDown() throws Exception {
        after();
    }
}
