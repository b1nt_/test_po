package org.labs.lab3.firefox;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.labs.lab3.GeckoBaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static org.junit.Assert.assertEquals;

/**
 * Created by Слава on 24.04.2017.
 */
public class TestGoToPayPage extends GeckoBaseTest{

    @Before
    public void setUp() throws Exception {
        initAndLogin();
    }

    @Test
    public void test1() throws Exception {
        driver.findElement(By.xpath(".//*[@id='currency_menu']/li[2]/a")).click();

        driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='payer']/form/div/div[2]/input")));
        try {
            assertEquals("Пополнить баланс", driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[1]")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @After
    public void tearDown() throws Exception {
        after();
    }
}
