package org.labs.lab3.firefox;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.labs.lab3.GeckoBaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static org.junit.Assert.assertEquals;

/**
 * Created by Слава on 24.04.2017.
 */
public class TestHideNickPanel extends GeckoBaseTest {

    @Before
    public void setUp() throws Exception {
       initAndLogin();
    }

    @Test
    public void test1() throws Exception {
        driver.findElement(By.xpath("html/body/div[2]/div/div[1]/div[1]/div/div")).click();
        driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/div[2]/header/ul/li[6]/a")));
        try {
            assertEquals("curtain  closed" ,driver.findElement(By.xpath("html/body/div[2]/div/div[1]/div[1]")).getAttribute("class"));
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @After
    public void tearDown() throws Exception {
        after();
    }
}
