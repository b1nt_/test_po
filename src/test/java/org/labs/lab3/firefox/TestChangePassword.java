package org.labs.lab3.firefox;

import org.junit.*;
import static org.junit.Assert.*;

import org.labs.lab3.GeckoBaseTest;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Created by Слава on 24.04.2017.
 */
public class TestChangePassword extends GeckoBaseTest {

    @Before
    public void setUp() throws Exception {
        initAndLogin();
    }

    @Test
    public void test_positive() throws Exception {
        driver.findElement(By.xpath(".//*[@id='profile_menu']/li[2]/a")).click();

        WebElement oldpass = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/div[1]/input"));
        jse.executeScript("arguments[0].value='" + profile.getPassword() + "';", oldpass);

        WebElement newpass = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/div[2]/input"));
        String newPass = Long.toString(Math.round(Math.random()*1111111));
        System.err.println(newPass);
        jse.executeScript("arguments[0].value='" + newPass + "';", newpass);

        WebElement newpassag = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/div[3]/input"));
        jse.executeScript("arguments[0].value='" + newPass + "';", newpassag);

        driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/input")).click();

        driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/input")));
        try {
            assertEquals("Новый пароль успешно установлен", driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/p[2]")).getText());
            myIO.write(profile.getEmail(), newPass);
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @Test
    public void test_negative_with_old_password() throws Exception {
        driver.findElement(By.xpath(".//*[@id='profile_menu']/li[2]/a")).click();

        WebElement oldpass = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/div[1]/input"));
        jse.executeScript("arguments[0].value='1111111';", oldpass);

        WebElement newpass = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/div[2]/input"));
        String newPass = Long.toString(Math.round(Math.random()*1111111));
        jse.executeScript("arguments[0].value='" + newPass + "';", newpass);

        WebElement newpassag = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/div[3]/input"));
        jse.executeScript("arguments[0].value='" + newPass + "';", newpassag);

        driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/input")).click();

        driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/input")));
        try {
            assertEquals("Старый пароль заполнен неверно", driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/p[2]")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @Test
    public void test_negative_with_newpass_is_invalid() throws Exception {
        driver.findElement(By.xpath(".//*[@id='profile_menu']/li[2]/a")).click();

        WebElement oldpass = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/div[1]/input"));
        jse.executeScript("arguments[0].value='" + profile.getPassword() + "';", oldpass);

        WebElement newpass = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/div[2]/input"));
        String newPass = Long.toString(Math.round(Math.random()*1111));
        jse.executeScript("arguments[0].value='" + newPass + "';", newpass);

        WebElement newpassag = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/div[3]/input"));
        jse.executeScript("arguments[0].value='" + newPass + "';", newpassag);

        driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/input")).click();

        driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/input")));
        try {
            assertEquals("Новый пароль имеет неверный формат", driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/p[2]")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @Test
    public void test_negative_with_newpass_is_valid_and_do_not_match() throws Exception {
        driver.findElement(By.xpath(".//*[@id='profile_menu']/li[2]/a")).click();

        WebElement oldpass = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/div[1]/input"));
        jse.executeScript("arguments[0].value='" + profile.getPassword() + "';", oldpass);

        WebElement newpass = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/div[2]/input"));
        String newPass = Long.toString(Math.round(Math.random()*1111111));
        jse.executeScript("arguments[0].value='" + newPass + "';", newpass);

        WebElement newpassag = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/div[3]/input"));
        jse.executeScript("arguments[0].value='" + newPass + 1 + "';", newpassag);

        driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/input")).click();

        driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/input")));
        try {
            assertEquals("Пароль и повтор пароля не совпадают", driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/p[2]")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @Test
    public void test_negative_with_newpass_not_valid_and_do_not_match() throws Exception {
        driver.findElement(By.xpath(".//*[@id='profile_menu']/li[2]/a")).click();

        WebElement oldpass = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/div[1]/input"));
        jse.executeScript("arguments[0].value='" + profile.getPassword() + "';", oldpass);

        WebElement newpass = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/div[2]/input"));
        String newPass = Long.toString(Math.round(Math.random()*111111));
        jse.executeScript("arguments[0].value='" + newPass + "';", newpass);

        WebElement newpassag = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/div[3]/input"));
        jse.executeScript("arguments[0].value='" + newPass + 1 + "';", newpassag);

        driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/input")).click();

        driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/input")));
        try {
            assertEquals("Новый пароль имеет неверный формат", driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/p[2]")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @Test
    public void test_negative_without_newpass() throws Exception {
        driver.findElement(By.xpath(".//*[@id='profile_menu']/li[2]/a")).click();

        WebElement oldpass = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/div[1]/input"));
        jse.executeScript("arguments[0].value='" + profile.getPassword() + "';", oldpass);

        String newPass = Long.toString(Math.round(Math.random()*1111111));

        WebElement newpassag = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/div[3]/input"));
        jse.executeScript("arguments[0].value='" + newPass + "';", newpassag);

        driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/input")).click();

        driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/form/div/input")));
        try {
            assertEquals("Ваш пароль должен содержать от 6 до 20 символов (только англ. символы)", driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/p[2]")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @After
    public void tearDown() throws Exception {
       after();
    }
}