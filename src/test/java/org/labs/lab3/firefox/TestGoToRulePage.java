package org.labs.lab3.firefox;

import org.junit.*;
import static org.junit.Assert.*;

import org.junit.Test;
import org.labs.lab3.GeckoBaseTest;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class TestGoToRulePage extends GeckoBaseTest {

	@Before
	public void setUp() throws Exception {
		initWithoutLogin();
	}

	@Test
	public void test1() throws Exception {
		driver.findElement(By.xpath("html/body/div[2]/header/ul/li[3]/a")).click();

		driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/div[2]/header/ul/li[3]/a")));
		try {
            assertEquals("Правила / goldenbirds.biz", driver.getTitle());
		} catch (Error e) {
			verificationErrors.append(e.toString());
		}
	}

	@After
	public void tearDown() throws Exception {
		after();
	}
}