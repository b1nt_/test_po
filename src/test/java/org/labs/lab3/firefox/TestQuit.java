package org.labs.lab3.firefox;

import org.labs.lab3.GeckoBaseTest;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.*;

/**
 * Created by Слава on 23.04.2017.
 */
public class TestQuit extends GeckoBaseTest{

    @Before
    public void setUp() throws Exception {
      initAndLogin();
    }

    @Test
    public void test1() throws Exception {
        driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/div[2]/header/ul/li[6]/a")));
        driver.findElement(By.xpath(".//*[@id='profile_menu']/li[8]/form/input")).click();

        driverWait.until(ExpectedConditions.urlToBe("http://goldenbirds.biz/rules"));
        try {
            assertEquals("Правила / goldenbirds.biz", driver.getTitle());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @After
    public void tearDown() throws Exception {
        after();
    }
}
