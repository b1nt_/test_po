package org.labs.lab3;

import org.labs.lab3.utils.Profile;

/**
 * Created by b1nt_ on 08.05.2017.
 */
public class GeckoBaseTest extends BaseTest {
    private final String nameBrowser = "gecko";
    private final String baseUrl = "http://goldenbirds.biz/";

    protected final Profile profile = new Profile(myIO);

    public void initWithoutLogin() {
        init(nameBrowser, baseUrl);
    }

    public void initAndLogin() {
        init(nameBrowser, baseUrl, profile);
    }
}
