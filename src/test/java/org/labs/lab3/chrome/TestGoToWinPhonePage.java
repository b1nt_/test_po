package org.labs.lab3.chrome;

import org.junit.*;
import static org.junit.Assert.*;

import org.junit.Test;
import org.labs.lab3.ChromeBaseTest;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Created by Слава on 24.04.2017.
 */
public class TestGoToWinPhonePage extends ChromeBaseTest {

    @Before
    public void setUp() throws Exception {
        initAndLogin();
    }

    @Test
    public void test1() throws Exception {
        driver.findElement(By.xpath("html/body/div[2]/div/div[1]/div[2]/ul/li/a")).click();

        driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[3]/div/div[2]/div/a")));
        try {
            assertEquals("Выиграй Iphone / goldenbirds.biz", driver.getTitle());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @After
    public void tearDown() throws Exception {
        after();
    }
}
