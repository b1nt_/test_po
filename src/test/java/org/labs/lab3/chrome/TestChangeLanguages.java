package org.labs.lab3.chrome;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.labs.lab3.ChromeBaseTest;
import org.openqa.selenium.By;

import static org.junit.Assert.*;

/**
 * Created by Слава on 23.04.2017.
 */
public class TestChangeLanguages extends ChromeBaseTest {

    @Before
    public void setUp() throws Exception {
        initWithoutLogin();
    }

    @Test
    public void test1() throws Exception {
        driver.findElement(By.xpath("html/body/div[2]/header/div[1]/div/div[2]")).click();
        driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();
        driver.findElement(By.xpath("html/body/div[2]/header/div[1]/div/div[2]")).click();

        try {
            assertTrue(driver.findElement(By.xpath(".//*[@id='languages']/div[2]/form[2]/img")).isDisplayed());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @After
    public void tearDown() throws Exception {
        after();
    }

}