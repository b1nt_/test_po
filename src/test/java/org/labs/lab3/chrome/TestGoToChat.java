package org.labs.lab3.chrome;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.labs.lab3.ChromeBaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static org.junit.Assert.assertEquals;

/**
 * Created by Слава on 24.04.2017.
 */
public class TestGoToChat extends ChromeBaseTest {

    @Before
    public void setUp() throws Exception {
      initAndLogin();
    }

    @Test
    public void test1() throws Exception {
        driver.findElement(By.xpath(".//*[@id='profile_menu']/li[7]/a")).click();
        driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='my_menu']/div[1]/i")));
        try {
            assertEquals("Golden Birds", driver.getTitle());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @After
    public void tearDown() throws Exception {
        after();
    }
}
