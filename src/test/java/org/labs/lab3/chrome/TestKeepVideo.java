package org.labs.lab3.chrome;

import org.junit.*;
import static org.junit.Assert.*;

import org.labs.lab3.ChromeBaseTest;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Created by Слава on 24.04.2017.
 */
public class TestKeepVideo extends ChromeBaseTest {

    @Before
    public void setUp() throws Exception {
       initAndLogin();
    }

    @Test
    public void test1() throws Exception {
        driver.findElement(By.xpath(".//*[@id='profile_menu']/li[5]/a")).click();

        driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/div[2]/div/div[2]/div[2]/form/textarea")));
        WebElement text = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/form/textarea"));
        jse.executeScript("arguments[0].value='https://www.youtube.com/';", text);

        WebElement button = driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/form/input[2]"));
        button.click();

        driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/div[2]/div/div[2]/div[2]/form/input[2]")));
        try {
            assertEquals("Вы ввели неверный адрес видео.", driver.findElement(By.xpath("html/body/div[2]/div/div[2]/div[2]/div[4]")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @After
    public void tearDown() throws Exception {
        after();
    }
}
