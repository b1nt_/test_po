package org.labs.lab3.chrome;

import org.labs.lab3.ChromeBaseTest;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.*;

/**
 * Created by Слава on 23.04.2017.
 */
public class TestLogIn extends ChromeBaseTest {

    @Before
    public void setUp() throws Exception {
       initAndLogin();
    }

    @Test
    public void test1() throws Exception {
        driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/div[2]/header/ul/li[6]/a")));
        try {
            assertEquals("Профиль / goldenbirds.biz", driver.getTitle());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @After
    public void tearDown() throws Exception {
        after();
    }
}
