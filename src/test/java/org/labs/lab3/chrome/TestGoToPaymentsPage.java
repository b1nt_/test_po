package org.labs.lab3.chrome;

import org.junit.*;
import static org.junit.Assert.*;

import org.junit.Test;
import org.labs.lab3.ChromeBaseTest;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Created by Слава on 23.04.2017.
 */

public class TestGoToPaymentsPage extends ChromeBaseTest {

    @Before
    public void setUp() throws Exception {
        initWithoutLogin();
    }

    @Test
    public void test1() throws Exception {
        driver.findElement(By.xpath("html/body/div[2]/header/ul/li[6]/a")).click();

        driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/div[2]/header/ul/li[6]/a")));
        try {
            assertEquals("Последние выплаты / goldenbirds.biz", driver.getTitle());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @After
    public void tearDown() throws Exception {
        after();
    }
}