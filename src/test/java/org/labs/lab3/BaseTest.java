package org.labs.lab3;

import org.labs.lab3.utils.MyIO;
import org.labs.lab3.utils.Profile;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

/**
 * Created by b1nt_ on 29.04.2017.
 */
public class BaseTest {
    protected WebDriver driver;
    protected WebDriverWait driverWait;
    protected StringBuffer verificationErrors = new StringBuffer();
    protected MyIO myIO = new MyIO();
    protected JavascriptExecutor jse;

    public WebDriver setDriver(String nameBrowser) {
        System.setProperty("webdriver." + nameBrowser + ".driver", "resources\\" + nameBrowser + "driver.exe");
        switch (nameBrowser) {
            case "gecko":
                return new FirefoxDriver();
            case "chrome":
                return new ChromeDriver();
            default:
                return null;
        }
    }

    public void init(String nameBrowser, String baseUrl) {
        driver = setDriver(nameBrowser);
        driverWait = new WebDriverWait(driver, 100);
        jse = (JavascriptExecutor)driver;

        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(100, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(100, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        driver.get(baseUrl);
    }

    public void init(String nameBrowser, String baseUrl, Profile profile) {
        init(nameBrowser, baseUrl);

        login(profile);
    }

    public void after() {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    public void checkLang () {
        Set<Cookie> cookies = driver.manage().getCookies();
        Iterator<Cookie> iterator = cookies.iterator();
        while (iterator.hasNext()) {
           if (iterator.next().getValue().equals("RU")) {
               driver.findElement(By.xpath("html/body/div[2]/header/div[1]/div/div[2]")).click();
               driver.findElement(By.xpath("(//button[@type='submit'])[1]")).click();
           }
        }
    }

    public void login(Profile profile) {
        driver.findElement(By.xpath("//input[@value='Войти']")).click();

        WebElement element = driver.findElement(By.xpath(".//*[@id='log_form']/input[1]"));
        WebElement element1 = driver.findElement(By.xpath(".//*[@id='log_form']/input[2]"));

        jse.executeScript("arguments[0].value='" + profile.getEmail() + "';", element);
        jse.executeScript("arguments[0].value='" + profile.getPassword() + "';", element1);
        driver.findElement(By.xpath(".//*[@id='recaptcha-submit']")).click();

        driverWait.until(ExpectedConditions.urlToBe("http://goldenbirds.biz/profile"));
    }
}
