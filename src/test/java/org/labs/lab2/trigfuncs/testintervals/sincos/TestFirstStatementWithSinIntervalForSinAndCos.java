package org.labs.lab2.trigfuncs.testintervals.sincos;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.labs.lab2.system.statement.FirstStatement;
import org.labs.lab2.system.statement.functions.trigfuncs.*;

import java.util.Arrays;
import java.util.Collection;

import static org.labs.lab2.utils.MathUtil.delta;

/**
 * Created by Слава on 15.04.2017.
 */
@RunWith(Parameterized.class)
public class TestFirstStatementWithSinIntervalForSinAndCos {

    private double input;
    private double expected;

    private Sin sin;
    private Cos cos;
    private Sec sec;
    private Cot cot;
    private Tan tan;

    private FirstStatement statement;

    public TestFirstStatementWithSinIntervalForSinAndCos(double input, double expected) {
        this.input = input;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> input_data() {
        return Arrays.asList(new Object[][]{
                {-3*Math.PI/4, -7.68198},
                {-7*Math.PI/4, -1.31801},
                {-4*Math.PI/3, -0.06282},
                {-Math.PI/5, 0.29842}
        });
    }

    @Before
    public void initTest () throws Exception {
        sin = new Sin();
        cos = new Cos();
        sec = new Sec();
        cot = new Cot();
        tan = new Tan();
        statement = new FirstStatement(sin, cos, sec, cot, tan);
    }

    @Test
    public void test () throws Exception {
        double result = statement.exc(input);

        if (Double.isInfinite(result) || Double.isNaN(result)) {
            Assert.fail();
        }
        Assert.assertEquals(expected, result, delta);
    }
}
