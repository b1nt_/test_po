package org.labs.lab2.trigfuncs.testintervals.tancotsec;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.labs.lab2.system.statement.FirstStatement;
import org.labs.lab2.system.statement.functions.trigfuncs.*;

import java.util.Arrays;
import java.util.Collection;

import static org.labs.lab2.utils.MathUtil.delta;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Слава on 14.04.2017.
 */
@RunWith(Parameterized.class)
public class TestFirstStatementWithoutAllIntervalForTanAndCotAndSec {

    private double input;
    private double expected;

    private Sin sin;
    private Cos cos;
    private Sec sec;
    private Cot cot;
    private Tan tan;

    private FirstStatement statement;

    public TestFirstStatementWithoutAllIntervalForTanAndCotAndSec(double input, double expected) {
        this.input = input;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> input_data() {
        return Arrays.asList(new Object[][]{
                {-Math.PI/4, -0.14645},
                {-Math.PI/3, -0.04487},
                {-3*Math.PI/4, -7.68198},
                {-5*Math.PI/6, -5.97527},
                {-6*Math.PI/5, -1.41898},
                {-5*Math.PI/4, -0.85355},
                {-8*Math.PI/5, -22.56001},
                {-7*Math.PI/4, -1.31802}
        });
    }

    @Before
    public void initTest () throws Exception {
        sin = mock(Sin.class);
        cos = mock(Cos.class);
        sec = mock(Sec.class);
        cot = mock(Cot.class);
        tan = mock(Tan.class);
        statement = new FirstStatement(sin, cos, sec, cot, tan);

        when(sin.exc(-Math.PI/4)).thenReturn(-0.70711);
        when(sin.exc(-Math.PI/3)).thenReturn(-0.86602);
        when(sin.exc(-3*Math.PI/4)).thenReturn(-0.70711);
        when(sin.exc(-5*Math.PI/6)).thenReturn(-0.5);
        when(sin.exc(-6*Math.PI/5)).thenReturn(0.58779);
        when(sin.exc(-5*Math.PI/4)).thenReturn(0.70711);
        when(sin.exc(-8*Math.PI/5)).thenReturn(0.95106);
        when(sin.exc(-7*Math.PI/4)).thenReturn(0.70711);

        when(cos.exc(-Math.PI/4)).thenReturn(0.70711);
        when(cos.exc(-Math.PI/3)).thenReturn(0.5);
        when(cos.exc(-3*Math.PI/4)).thenReturn(-0.70711);
        when(cos.exc(-5*Math.PI/6)).thenReturn(-0.86603);
        when(cos.exc(-6*Math.PI/5)).thenReturn(-0.80902);
        when(cos.exc(-5*Math.PI/4)).thenReturn(-0.70711);
        when(cos.exc(-8*Math.PI/5)).thenReturn(0.30902);
        when(cos.exc(-7*Math.PI/4)).thenReturn(0.70711);

        when(sec.exc(-Math.PI/4)).thenReturn(1.41421);
        when(sec.exc(-Math.PI/3)).thenReturn(2.0);
        when(sec.exc(-3*Math.PI/4)).thenReturn(-1.41421);
        when(sec.exc(-5*Math.PI/6)).thenReturn(-1.1547);
        when(sec.exc(-6*Math.PI/5)).thenReturn(-1.23607);
        when(sec.exc(-5*Math.PI/4)).thenReturn(-1.41421);
        when(sec.exc(-8*Math.PI/5)).thenReturn(3.23607);
        when(sec.exc(-7*Math.PI/4)).thenReturn(1.41421);

        when(cot.exc(-Math.PI/4)).thenReturn(-1.0);
        when(cot.exc(-Math.PI/3)).thenReturn(-0.57735);
        when(cot.exc(-3*Math.PI/4)).thenReturn(1.0);
        when(cot.exc(-5*Math.PI/6)).thenReturn(1.73205);
        when(cot.exc(-6*Math.PI/5)).thenReturn(-1.37638);
        when(cot.exc(-5*Math.PI/4)).thenReturn(-1.0);
        when(cot.exc(-8*Math.PI/5)).thenReturn(0.32492);
        when(cot.exc(-7*Math.PI/4)).thenReturn(1.0);

        when(tan.exc(-Math.PI/4)).thenReturn(-1.0);
        when(tan.exc(-Math.PI/3)).thenReturn(-1.73205);
        when(tan.exc(-3*Math.PI/4)).thenReturn(1.0);
        when(tan.exc(-5*Math.PI/6)).thenReturn(0.57735);
        when(tan.exc(-6*Math.PI/5)).thenReturn(-0.72654);
        when(tan.exc(-5*Math.PI/4)).thenReturn(-1.0);
        when(tan.exc(-8*Math.PI/5)).thenReturn(3.07768);
        when(tan.exc(-7*Math.PI/4)).thenReturn(1.0);
    }

    @Test
    public void test () throws Exception {
        double result = statement.exc(input);

        if (Double.isInfinite(result) || Double.isNaN(result)) {
            Assert.fail();
        }
        Assert.assertEquals(expected, result, delta);
    }
}
