package org.labs.lab2.trigfuncs.testpoints;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.labs.lab2.system.statement.FirstStatement;
import org.labs.lab2.system.statement.functions.trigfuncs.*;

import java.util.Arrays;
import java.util.Collection;

import static org.labs.lab2.utils.MathUtil.d;
import static org.labs.lab2.utils.MathUtil.delta;

/**
 * Created by Слава on 14.04.2017.
 */
@RunWith(Parameterized.class)
public class TestFirstStatementWithSinWithPoint {

    private double input;
    private double expected;

    private Sin sin;
    private Cos cos;
    private Sec sec;
    private Cot cot;
    private Tan tan;

    private FirstStatement statement;

    public TestFirstStatementWithSinWithPoint(double input, double expected) {
        this.input = input;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> input_data() {
        return Arrays.asList(new Object[][]{
                {0.0 + d, Double.NaN},
                {0.0, Double.NaN},
                {0.0 - d, 3.95911},
                {-Math.PI/2 + d, -9602.37677},
                {-Math.PI/2, Double.NEGATIVE_INFINITY},
                {-Math.PI/2 - d, -10402.3},
                {-3*Math.PI/2 + d, -9602.4},
                {-3*Math.PI/2, Double.NEGATIVE_INFINITY},
                {-3*Math.PI/2 - d, -10402.3},
                {-Math.PI + d, -4.0399},
                {-Math.PI, Double.NEGATIVE_INFINITY},
                {-Math.PI - d, -3.9599},
                {-2*Math.PI + d, 4.03909},
                {-2*Math.PI, Double.NaN},
                {-2*Math.PI - d, 3.95911},
        });
    }

    @Before
    public void initTest () throws Exception {
        sin = new Sin();
        cos = new Cos();
        sec = new Sec();
        cot = new Cot();
        tan = new Tan();
        statement = new FirstStatement(sin, cos, sec, cot, tan);
    }

    @Test
    public void test () throws Exception {
        double result = statement.exc(input);

        if (Double.isNaN(expected) || Double.isInfinite(expected)) {
            Assert.assertTrue(Double.isInfinite(result) || Double.isNaN(result));
        }  else {
            Assert.assertEquals(expected, result, delta);
        }
    }
}
