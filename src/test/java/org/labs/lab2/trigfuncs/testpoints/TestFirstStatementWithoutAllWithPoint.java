package org.labs.lab2.trigfuncs.testpoints;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.labs.lab2.system.statement.FirstStatement;
import org.labs.lab2.system.statement.functions.trigfuncs.*;

import java.util.Arrays;
import java.util.Collection;

import static org.labs.lab2.utils.MathUtil.d;
import static org.labs.lab2.utils.MathUtil.delta;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Слава on 14.04.2017.
 */
@RunWith(Parameterized.class)
public class TestFirstStatementWithoutAllWithPoint {

    private double input;
    private double expected;

    private Sin sin;
    private Cos cos;
    private Sec sec;
    private Cot cot;
    private Tan tan;

    private FirstStatement statement;

    public TestFirstStatementWithoutAllWithPoint(double input, double expected) {
        this.input = input;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> input_data() {
        return Arrays.asList(new Object[][]{
                {0.0 + d, Double.NaN},
                {0.0, Double.NaN},
                {0.0 - d, 3.95911},
                {-Math.PI/2 + d, -9602.37677},
                {-Math.PI/2, Double.NEGATIVE_INFINITY},
                {-Math.PI/2 - d, -10402.3},
                {-3*Math.PI/2 + d, -9602.4},
                {-3*Math.PI/2, Double.NEGATIVE_INFINITY},
                {-3*Math.PI/2 - d, -10402.3},
                {-Math.PI + d, -4.0399},
                {-Math.PI, Double.NEGATIVE_INFINITY},
                {-Math.PI - d, -3.9599},
                {-2*Math.PI + d, 4.03909},
                {-2*Math.PI, Double.NaN},
                {-2*Math.PI - d, 3.95911},
        });
    }
    
    @Before
    public void initTest () throws Exception {
        sin = mock(Sin.class);
        cos = mock(Cos.class);
        sec = mock(Sec.class);
        cot = mock(Cot.class);
        tan = mock(Tan.class);
        statement = new FirstStatement(sin, cos, sec, cot, tan);

        when(sin.exc(0.0)).thenReturn(0.0);
        when(sin.exc(0.0 - d)).thenReturn(-0.0099998);
        when(sin.exc(-Math.PI/2 + d)).thenReturn(-0.99995);
        when(sin.exc(-Math.PI/2)).thenReturn(-1.0);
        when(sin.exc(-Math.PI/2 - d)).thenReturn(-0.99995);
        when(sin.exc(-3*Math.PI/2 + d)).thenReturn(0.99995);
        when(sin.exc(-3*Math.PI/2)).thenReturn(1.0);
        when(sin.exc(-3*Math.PI/2 - d)).thenReturn(0.99995);
        when(sin.exc(-Math.PI + d)).thenReturn(-0.0099998);
        when(sin.exc(-Math.PI)).thenReturn(-1.22465);
        when(sin.exc(-Math.PI - d)).thenReturn(0.0099998);
        when(sin.exc(-2*Math.PI + d)).thenReturn(0.0099998);
        when(sin.exc(-2*Math.PI)).thenReturn(0.0);
        when(sin.exc(-2*Math.PI - d)).thenReturn(-0.0099998);

        when(cos.exc(0.0)).thenReturn(1.0);
        when(cos.exc(0.0 - d)).thenReturn(0.99995);
        when(cos.exc(-Math.PI/2 + d)).thenReturn(0.0099998);
        when(cos.exc(-Math.PI/2)).thenReturn(0.0);
        when(cos.exc(-Math.PI/2 - d)).thenReturn(-0.0099998);
        when(cos.exc(-3*Math.PI/2 + d)).thenReturn(-0.0099998);
        when(cos.exc(-3*Math.PI/2)).thenReturn(0.0);
        when(cos.exc(-3*Math.PI/2 - d)).thenReturn(0.0099998);
        when(cos.exc(-Math.PI + d)).thenReturn(-0.99995);
        when(cos.exc(-Math.PI)).thenReturn(-1.0);
        when(cos.exc(-Math.PI - d)).thenReturn(-0.99995);
        when(cos.exc(-2*Math.PI + d)).thenReturn(0.99995);
        when(cos.exc(-2*Math.PI)).thenReturn(1.0);
        when(cos.exc(-2*Math.PI - d)).thenReturn(0.99995);

        when(sec.exc(0.0)).thenReturn(1.0);
        when(sec.exc(0.0 - d)).thenReturn(1.00005);
        when(sec.exc(-Math.PI/2 + d)).thenReturn(100.002);
        when(sec.exc(-Math.PI/2)).thenReturn(Double.NEGATIVE_INFINITY);
        when(sec.exc(-Math.PI/2 - d)).thenReturn(-100.002);
        when(sec.exc(-3*Math.PI/2 + d)).thenReturn(-100.002);
        when(sec.exc(-3*Math.PI/2)).thenReturn(Double.NEGATIVE_INFINITY);
        when(sec.exc(-3*Math.PI/2 - d)).thenReturn(100.002);
        when(sec.exc(-Math.PI + d)).thenReturn(-1.00005);
        when(sec.exc(-Math.PI)).thenReturn(-1.0);
        when(sec.exc(-Math.PI - d)).thenReturn(-1.00005);
        when(sec.exc(-2*Math.PI + d)).thenReturn(1.00005);
        when(sec.exc(-2*Math.PI)).thenReturn(1.0);
        when(sec.exc(-2*Math.PI - d)).thenReturn(1.00005);

        when(cot.exc(0.0)).thenReturn(Double.NEGATIVE_INFINITY);
        when(cot.exc(0.0 - d)).thenReturn(-99.9967);
        when(cot.exc(-Math.PI/2 + d)).thenReturn(-0.01);
        when(cot.exc(-Math.PI/2)).thenReturn(0.0);
        when(cot.exc(-Math.PI/2 - d)).thenReturn(0.01);
        when(cot.exc(-3*Math.PI/2 + d)).thenReturn(-0.01);
        when(cot.exc(-3*Math.PI/2)).thenReturn(0.0);
        when(cot.exc(-3*Math.PI/2 - d)).thenReturn(0.01);
        when(cot.exc(-Math.PI + d)).thenReturn(99.9967);
        when(cot.exc(-Math.PI)).thenReturn(Double.NEGATIVE_INFINITY);
        when(cot.exc(-Math.PI - d)).thenReturn(-99.9967);
        when(cot.exc(-2*Math.PI + d)).thenReturn(99.9967);
        when(cot.exc(-2*Math.PI)).thenReturn(Double.NEGATIVE_INFINITY);
        when(cot.exc(-2*Math.PI - d)).thenReturn(-99.9967);

        when(tan.exc(0.0)).thenReturn(0.0);
        when(tan.exc(0.0 - d)).thenReturn(-0.01);
        when(tan.exc(-Math.PI/2 + d)).thenReturn(-99.9967);
        when(tan.exc(-Math.PI/2)).thenReturn(Double.NEGATIVE_INFINITY);
        when(tan.exc(-Math.PI/2 - d)).thenReturn(99.9967);
        when(tan.exc(-3*Math.PI/2 + d)).thenReturn(-99.9967);
        when(tan.exc(-3*Math.PI/2)).thenReturn(Double.NEGATIVE_INFINITY);
        when(tan.exc(-3*Math.PI/2 - d)).thenReturn(99.9967);
        when(tan.exc(-Math.PI + d)).thenReturn(0.01);
        when(tan.exc(-Math.PI)).thenReturn(0.0);
        when(tan.exc(-Math.PI - d)).thenReturn(-0.01);
        when(tan.exc(-2*Math.PI + d)).thenReturn(0.01);
        when(tan.exc(-2*Math.PI)).thenReturn(0.0);
        when(tan.exc(-2*Math.PI - d)).thenReturn(-0.01);
    }
    
    @Test
    public void test () throws Exception {
        double result = statement.exc(input);

        if (Double.isNaN(expected) || Double.isInfinite(expected)) {
            Assert.assertTrue(Double.isInfinite(result) || Double.isNaN(result));
        }  else {
            Assert.assertEquals(expected, result, delta);
        }
    }
}
