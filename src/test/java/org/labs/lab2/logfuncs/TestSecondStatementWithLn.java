package org.labs.lab2.logfuncs;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.labs.lab2.system.statement.SecondStatement;
import org.labs.lab2.system.statement.functions.logfuncs.Ln;
import org.labs.lab2.system.statement.functions.logfuncs.Log10;
import org.labs.lab2.system.statement.functions.logfuncs.Log3;
import org.labs.lab2.system.statement.functions.logfuncs.Log5;

import java.util.Arrays;
import java.util.Collection;

import static org.labs.lab2.utils.MathUtil.d;
import static org.labs.lab2.utils.MathUtil.delta;

/**
 * Created by Слава on 14.04.2017.
 */
@RunWith(Parameterized.class)
public class TestSecondStatementWithLn {

    private double input;
    private double expected;

    private static Ln ln;
    private Log3 log3;
    private Log5 log5;
    private Log10 log10;

    private SecondStatement statement;

    public TestSecondStatementWithLn(double input, double expected) {
        this.input = input;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> input_data() {
        return Arrays.asList(new Object[][]{
                {0.0, Double.NaN},
                {0.0 + d, 3.91309},
                {0.5, 1.17407},
                {1.0 - d,0.9103},
                {1.0 , Double.NaN},
                {1.0 + d, 0.9103},
                {1.259 , 0.94447},
                {2 , 1.24345}
        });
    }

    @Before
    public void initTest() {
        ln = new Ln();
        log3 = new Log3();
        log5 = new Log5();
        log10 = new Log10();
        statement = new SecondStatement(ln, log3, log5, log10);
    }

    @Test
    public void test () throws Exception {
        double result = statement.exc(input);

        if (Double.isNaN(expected)) {
            Assert.assertTrue(Double.isNaN(result));
        } else if (Double.isNaN(result)) {
            Assert.fail();
        } else {
            Assert.assertEquals(expected, result, delta);
        }
    }
}
