package org.labs.lab2.logfuncs;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.labs.lab2.system.statement.SecondStatement;
import org.labs.lab2.system.statement.functions.logfuncs.Ln;
import org.labs.lab2.system.statement.functions.logfuncs.Log10;
import org.labs.lab2.system.statement.functions.logfuncs.Log3;
import org.labs.lab2.system.statement.functions.logfuncs.Log5;

import java.util.Arrays;
import java.util.Collection;

import static org.labs.lab2.utils.MathUtil.delta;
import static org.labs.lab2.utils.MathUtil.d;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Слава on 12.04.2017.
 */
@RunWith(Parameterized.class)
public class TestSecondStatementWithoutAll {

    private double input;
    private double expected;

    private static Ln ln;
    private Log3 log3;
    private Log5 log5;
    private Log10 log10;

    private SecondStatement statement;

    public TestSecondStatementWithoutAll (double input, double expected) {
        this.input = input;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> input_data () {
        return Arrays.asList(new Object[][]{
                {0.0, Double.NaN},
                {0.0 + d, 3.91309},
                {0.5, 1.17407},
                {1.0 - d,0.9103},
                {1.0 , Double.NaN},
                {1.0 + d, 0.9103},
                {1.259 , 0.94447},
                {2 , 1.24345}
        });
    }

    @Before
    public void initTest() {
        ln = mock(Ln.class);
        log3 = mock(Log3.class);
        log5 = mock(Log5.class);
        log10 = mock(Log10.class);
        statement = new SecondStatement(ln, log3, log5, log10);

        when(ln.exc(0.0)).thenReturn(Double.NEGATIVE_INFINITY);
        when(ln.exc(0.0 + d)).thenReturn(-4.60517);
        when(ln.exc(0.5)).thenReturn(-0.69315);
        when(ln.exc(1.0 - d)).thenReturn(-0.01005);
        when(ln.exc(1.0)).thenReturn(0.0);
        when(ln.exc(1.0 + d)).thenReturn(0.00995);
        when(ln.exc(1.259)).thenReturn(0.23032);
        when(ln.exc(2)).thenReturn(0.693147);

        when(log3.exc(0.0)).thenReturn(Double.NEGATIVE_INFINITY);
        when(log3.exc(0.0 + d)).thenReturn(-4.19181);
        when(log3.exc(0.5)).thenReturn(-0.63093);
        when(log3.exc(1.0 - d)).thenReturn(-0.00915);
        when(log3.exc(1.0)).thenReturn(0.0);
        when(log3.exc(1.0 + d)).thenReturn(0.00906);
        when(log3.exc(1.259)).thenReturn(0.20964);
        when(log3.exc(2)).thenReturn(0.63093);

        when(log5.exc(0.0)).thenReturn(Double.NEGATIVE_INFINITY);
        when(log5.exc(0.0 + d)).thenReturn(-2.86134);
        when(log5.exc(0.5)).thenReturn(-0.43068);
        when(log5.exc(1.0 - d)).thenReturn(-0.00624);
        when(log5.exc(1.0)).thenReturn(0.0);
        when(log5.exc(1.0 + d)).thenReturn(0.00618);
        when(log5.exc(1.259)).thenReturn(0.14310);
        when(log5.exc(2)).thenReturn(0.43068);

        when(log10.exc(0.0)).thenReturn(Double.NEGATIVE_INFINITY);
        when(log10.exc(0.0 + d)).thenReturn(-2.0);
        when(log10.exc(0.5)).thenReturn(-0.30103);
        when(log10.exc(1.0 - d)).thenReturn(-0.00436);
        when(log10.exc(1.0)).thenReturn(0.0);
        when(log10.exc(1.0 + d)).thenReturn(0.00432);
        when(log10.exc(1.259)).thenReturn(0.10003);
        when(log10.exc(2)).thenReturn(0.30103);
    }

    @Test
    public void test () throws Exception {
        double result = statement.exc(input);

        if (Double.isNaN(expected)) {
            Assert.assertTrue(Double.isNaN(result));
        } else if (Double.isNaN(result)) {
            Assert.fail();
        } else {
            Assert.assertEquals(expected, result, delta);
        }
    }
}
