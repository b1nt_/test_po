package org.labs.lab2.csv;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.labs.lab2.system.statement.FirstStatement;
import org.labs.lab2.system.statement.functions.trigfuncs.*;
import org.labs.lab2.utils.CSVWriter;

/**
 * Created by Слава on 15.04.2017.
 */
public class DoFuncFirst {

    private CSVWriter writer;
    private FirstStatement statement;

    @Before
    public void intiTest() {
        statement = new FirstStatement(new Sin(), new Cos(), new Sec(), new Cot(), new Tan());
        writer = new CSVWriter(statement);
    }

    @Ignore
    @Test(timeout = 10000)
    public void test () throws Exception {
        writer.write(-2*Math.PI,0.01, 0.3);
    }
}
