package org.labs.lab2.csv;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.labs.lab2.system.statement.SecondStatement;
import org.labs.lab2.system.statement.functions.logfuncs.Ln;
import org.labs.lab2.system.statement.functions.logfuncs.Log10;
import org.labs.lab2.system.statement.functions.logfuncs.Log3;
import org.labs.lab2.system.statement.functions.logfuncs.Log5;
import org.labs.lab2.utils.CSVWriter;

/**
 * Created by Слава on 15.04.2017.
 */
public class DoFunctSecond {

    private CSVWriter writer;
    private SecondStatement statement;

    @Before
    public void intiTest() {
        statement = new SecondStatement(new Ln(), new Log3(), new Log5(), new Log10());
        writer = new CSVWriter(statement);
    }

    @Ignore
    @Test(timeout = 10000)
    public void test () throws Exception {
        writer.write(0.01,5, 0.2);
    }
}
