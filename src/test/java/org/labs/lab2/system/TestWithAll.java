package org.labs.lab2.system;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.labs.lab2.system.statement.FirstStatement;
import org.labs.lab2.system.statement.SecondStatement;
import org.labs.lab2.system.statement.functions.logfuncs.Ln;
import org.labs.lab2.system.statement.functions.logfuncs.Log10;
import org.labs.lab2.system.statement.functions.logfuncs.Log3;
import org.labs.lab2.system.statement.functions.logfuncs.Log5;
import org.labs.lab2.system.statement.functions.trigfuncs.*;

import java.io.Writer;
import java.util.Arrays;
import java.util.Collection;

import static org.labs.lab2.utils.MathUtil.d;
import static org.labs.lab2.utils.MathUtil.delta;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Слава on 15.04.2017.
 */
@RunWith(Parameterized.class)
public class TestWithAll {

    private double input;
    private double expected;

    private FirstStatement firstStatement;
    private SecondStatement secondStatement;

    private Main main;

    public TestWithAll(double input, double expected) {
        this.input = input;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> input_data() {
        return Arrays.asList(new Object[][]{
                {0.0 - d, 3.95911},
                {0.0, Double.NaN},
                {0.0 + d, 3.91309},
                {0.5, 1.17407},
                {1.0 , Double.NaN},
                {1.259 , 0.94447},
                {2 , 1.24345},
                {-Math.PI/3, -0.04487},
                {-3*Math.PI/4, -7.68198},
                {-6*Math.PI/5, -1.41898},
                {-8*Math.PI/5, -22.56001},
                {-Math.PI/2, Double.NEGATIVE_INFINITY},
                {-2*Math.PI, Double.NaN},
        });
    }

    @Before
    public void initTest () {
        firstStatement = new FirstStatement(new Sin(), new Cos(), new Sec(), new Cot(), new Tan());
        secondStatement = new SecondStatement(new Ln(), new Log3(), new Log5(), new Log10());
        main = new Main(firstStatement, secondStatement);

    }

    @Test
    public void test () throws Exception {
        double result = main.exc(input);

        if (Double.isNaN(expected) || Double.isInfinite(expected)) {
            Assert.assertTrue(Double.isInfinite(result) || Double.isNaN(result));
        }  else {
            Assert.assertEquals(expected, result, delta);
        }
    }
}
