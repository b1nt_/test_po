package org.labs.lab3.utils;

/**
 * Created by b1nt_ on 29.04.2017.
 */
public class Profile {

    private String email;
    private String password;

    public Profile(MyIO myIO) {
        String[] profile = myIO.read();

        email = profile[0];
        password = profile[1];
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
