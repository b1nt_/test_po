package org.labs.lab3.utils;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Locale;

/**
 * Created by Слава on 24.04.2017.
 */
public class MyIO {
    private String SEPARATOR = ";";
    private String FILE_NAME = "data.txt";

    public void write(String email, String login) {
        try {
            FileWriter writer = new FileWriter(FILE_NAME, false);

            writer.append(String.format(Locale.US, "%s%s%s\n", email, SEPARATOR, login));

            writer.close();
        } catch (IOException e) {
            System.err.println(e);
        }
    }

    public String[] read() {
        try {
            List<String> lines = Files.readAllLines(Paths.get(FILE_NAME), StandardCharsets.UTF_8);
            String[] strings = lines.get(lines.size() - 1).split(SEPARATOR);
            return strings;
        } catch (IOException e) {
            System.err.println(e);
        }
        return null;
    }
}
