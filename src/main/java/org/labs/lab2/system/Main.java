package org.labs.lab2.system;

import org.labs.lab2.AbstractFunction;
import org.labs.lab2.system.statement.SecondStatement;
import org.labs.lab2.system.statement.FirstStatement;

/**
 * Created by Слава on 15.04.2017.
 */
public class Main extends AbstractFunction {

    private FirstStatement firstStatement;
    private SecondStatement secondStatement;

    public Main(FirstStatement firstStatement, SecondStatement secondStatement) {
        this.firstStatement = firstStatement;
        this.secondStatement = secondStatement;
    }

    @Override
    public double exc (double x) {
        if(x <= 0) {
            return firstStatement.exc(x);
        } else {
            return secondStatement.exc(x);
        }
    }
}
