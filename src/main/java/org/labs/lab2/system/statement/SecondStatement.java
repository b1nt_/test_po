package org.labs.lab2.system.statement;

import org.labs.lab2.AbstractFunction;
import org.labs.lab2.system.statement.functions.logfuncs.Ln;
import org.labs.lab2.system.statement.functions.logfuncs.Log10;
import org.labs.lab2.system.statement.functions.logfuncs.Log3;
import org.labs.lab2.system.statement.functions.logfuncs.Log5;

/**
 * Created by Слава on 11.04.2017.
 */
public class SecondStatement extends AbstractFunction {

    private Ln ln;
    private Log3 log3;
    private Log5 log5;
    private Log10 log10;

    public SecondStatement(Ln ln, Log3 log3, Log5 log5, Log10 log10) {
        this.ln = ln;
        this.log3 = log3;
        this.log5 = log5;
        this.log10 = log10;
    }

    @Override
    public double exc (double x) {
        if (x > 0) {
            return (((Math.pow(log5.exc(x), 3) * log10.exc(x)) + log3.exc(x)) / ln.exc(x)) + (ln.exc(x) * log5.exc(x));
        }
        else return Double.NaN;
    }
}
