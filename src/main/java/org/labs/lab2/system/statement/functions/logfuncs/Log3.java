package org.labs.lab2.system.statement.functions.logfuncs;

import org.labs.lab2.AbstractFunction;

/**
 * Created by Слава on 11.04.2017.
 */
public class Log3 extends AbstractFunction {

    private Ln ln = new Ln();

    @Override
    public double exc (double x) {
        return ln.exc(x) / ln.exc(3);
    }
}
