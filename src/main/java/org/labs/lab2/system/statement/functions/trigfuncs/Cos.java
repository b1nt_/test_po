package org.labs.lab2.system.statement.functions.trigfuncs;

import org.labs.lab2.AbstractFunction;

/**
 * Created by Слава on 11.04.2017.
 */
public class Cos extends AbstractFunction {

    private Sin sin = new Sin();

    @Override
    public double exc (double x) {
        return sin.exc(x + Math.PI / 2);
    }
}
