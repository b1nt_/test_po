package org.labs.lab2.system.statement.functions.trigfuncs;

import org.labs.lab2.AbstractFunction;

import static org.labs.lab2.utils.MathUtil.e;

/**
 * Created by Слава on 11.04.2017.
 */
public class Cot extends AbstractFunction {

    private Sin sin = new Sin();
    private Cos cos = new Cos();

    @Override
    public double  exc (double x) {
        if(x == 0) {
            return Double.POSITIVE_INFINITY;
        }

        double temp_x = Math.abs(x);
        while (temp_x > 0) {
            temp_x -= Math.PI;
        }

        if (Math.abs(temp_x) < e) {
            return x < 0  ? Double.POSITIVE_INFINITY : Double.NEGATIVE_INFINITY;
        }

        return cos.exc(x) / sin.exc(x);
    }
}
