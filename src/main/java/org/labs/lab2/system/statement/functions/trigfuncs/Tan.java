package org.labs.lab2.system.statement.functions.trigfuncs;

import org.labs.lab2.AbstractFunction;

import static org.labs.lab2.utils.MathUtil.e;

/**
 * Created by Слава on 11.04.2017.
 */
public class Tan extends AbstractFunction {

    private Sin sin = new Sin();
    private Cos cos = new Cos();

    @Override
    public double exc (double x) {
        double temp_x = Math.abs(x);
        int flag = 0;

        while (temp_x > 0) {
            temp_x -= Math.PI/2;
            flag++;
        }

        if (Math.abs(temp_x) < e && flag % 2 != 0) {
            return x > 0  ? Double.POSITIVE_INFINITY : Double.NEGATIVE_INFINITY;
        }

        return sin.exc(x) / cos.exc(x);
    }
}
