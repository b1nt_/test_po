package org.labs.lab2.system.statement.functions.trigfuncs;

import org.labs.lab2.AbstractFunction;

import static org.labs.lab2.utils.MathUtil.*;

/**
 * Created by Слава on 11.04.2017.
 */
public class Sin extends AbstractFunction {

    @Override
    public double exc (double x) {
        double result = 0.0;

        for (int i = 0; i < n; i++) {
            result += Math.pow(-1, i) * Math.pow(x, 2 * i + 1) / Fact(2 * i + 1);
        }

        return result;
    }
}
