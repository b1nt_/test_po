package org.labs.lab2.system.statement;

import org.labs.lab2.AbstractFunction;
import org.labs.lab2.system.statement.functions.trigfuncs.*;

/**
 * Created by Слава on 11.04.2017.
 */
public class FirstStatement extends AbstractFunction {

    private Sin sin;
    private Cos cos;
    private Sec sec;
    private Cot cot;
    private Tan tan;

    public FirstStatement(Sin sin, Cos cos, Sec sec, Cot cot, Tan tan) {
        this.sin = sin;
        this.cos = cos;
        this.sec = sec;
        this.cot = cot;
        this.tan = tan;
    }

    @Override
    public double exc (double x) {
        if ( x <= 0) {
            return ((((cot.exc(x)/sec.exc(x)) - tan.exc(x)) * sin.exc(x)) / cos.exc(x)) * Math.pow((sin.exc(x) + cos.exc(x) + cos.exc(x)), 2);
        }
        else return Double.NaN;
    }
}
