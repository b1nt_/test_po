package org.labs.lab2.utils;

/**
 * Created by Слава on 11.04.2017.
 */
public class MathUtil {

    public static final int n = 100;

    public static final double e = 0.00001;
    public static final double d = 0.01;
    public static final double delta = 0.1;

    public static double Fact(int n) {
        double r = 1;
        for (int i = 2; i <= n; ++i) {
            r *= i;
        }
        return r;
    }
}
