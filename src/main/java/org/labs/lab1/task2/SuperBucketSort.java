package org.labs.lab1.task2;

import java.util.*;

/**
 * Created by Слава on 12.03.2017.
 */
public class SuperBucketSort {

    public List<String> log = new ArrayList<String>();

    public void bucket(Integer[] array) {
        Integer buckets_size = array.length;

        // Determine maximum values
        Integer maxValue = max(array);
        log.add("Max value is " + maxValue);

        // Create buckets
        List<LinkedList<Integer>> buckets = new ArrayList<LinkedList<Integer>>(buckets_size);
        for (int i = 0; i < buckets_size; i++) {
            buckets.add(new LinkedList<Integer>());
        }
        log.add("Create " + buckets_size + " buckets");

        // Fill buckets
        Integer bucket_number = 0;
        for (int i = 0; i < array.length; i++) {
            bucket_number = (array[i] * buckets_size) / (maxValue + 1);
            log.add("Add " + array[i]);
            log.add("In " + bucket_number + " bucket");

            if (buckets.get(bucket_number).size() > 0) {
                log.add("Before adding. This bucket contains: " + buckets.get(bucket_number).toString());
            }

            buckets.get(bucket_number).add(array[i]);
            if (buckets.get(bucket_number).size() > 1) {
                Collections.sort(buckets.get(bucket_number));
            }

            log.add("After adding. This bucket contains: " + buckets.get(bucket_number).toString());
        }

        // Create sorted array
        int current_index = 0;
        int bucket_size = 0;
        bucket_number = 0;
        for (LinkedList<Integer> bucket : buckets) {
            bucket_size = bucket.size();
            for (int i = 0; i < bucket_size; i++) {
                log.add("Get " + bucket.getFirst());
                log.add("Of " + bucket_number + " bucket");
                log.add("Set this value on " + current_index + " index");

                array[current_index++] = bucket.poll();
            }
            bucket_number++;
        }
        log.add("Array after sort " + Arrays.toString(array));
    }

    private static int max(Integer[] array) {
        Integer maxValue = array[0];
        for (int i = 1; i < array.length; i++) {
            maxValue = Math.max(maxValue, array[i]);
        }

        return maxValue;
    }
}
