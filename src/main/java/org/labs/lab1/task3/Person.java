package org.labs.lab1.task3;

import java.util.ArrayList;

/**
 * Created by Слава on 20.03.2017.
 */
public class Person {
    private final String name;
    private ArrayList<Door> throughDoors = new ArrayList<Door>();
    private Location position;

    public Person(String name, Location position) {
        this.name = name;
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Door> getThroughDoors() {
        return throughDoors;
    }

    public Location getPosition() {
        return position;
    }

    public void walkThroughTheDoor(Door d) {
        throughDoors.add(d);
        position = d.personPassed(this);
    }

    public void getOutOfTheCar(Car c) {
        position = c.getPosition();
        c.deletePassenger(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (name != null ? !name.equals(person.name) : person.name != null) return false;
        if (throughDoors != null ? !throughDoors.equals(person.throughDoors) : person.throughDoors != null)
            return false;
        return position != null ? position.equals(person.position) : person.position == null;
    }
}
