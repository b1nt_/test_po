package org.labs.lab1.task3;

import java.util.ArrayList;

/**
 * Created by Слава on 20.03.2017.
 */
public class Car extends Location {
    private boolean isDrive;
    private ArrayList<Person> passengers;
    private Door destination;
    private Door position;

    public Car(ArrayList<Person> passengers) {
        this.passengers = passengers;
    }

    public ArrayList<Person> getPassengers() {
        return passengers;
    }

    public void deletePassenger(Person p) {
        passengers.remove(p);
    }

    public boolean isDrive() {
        return isDrive;
    }

    public Door getDestination() {
        return destination;
    }

    public Door getPosition() {
        return position;
    }

    public void move(Door d) {
        isDrive = true;
        destination = d;
    }

    public Door stop(TRIP t) {
        if(TRIP.NOT_LONG.equals(t)) {
            position = destination;
            destination = null;
            isDrive = false;
        }
        return position;
    }
}
