package org.labs.lab1.task3;

/**
 * Created by Слава on 20.03.2017.
 */
public class Thing {
    private final NAME name;
    private final MATERIAL material;

    public Thing(NAME name, MATERIAL material) {
        this.name = name;
        this.material = material;
    }

    public NAME getName() {
        return name;
    }

    public MATERIAL getMaterial() {
        return material;
    }
}
