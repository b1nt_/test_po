package org.labs.lab1.task3;

/**
 * Created by Слава on 20.03.2017.
 */
public class Light {
    private final Door belong;
    private final DIRECTION direction;
    private boolean isLight;

    public Light (Door belong, DIRECTION direction) {
        this.belong = belong;
        this.direction = direction;
    }

    public Door getBelong() {
        return belong;
    }

    public DIRECTION getDirection() {
        return direction;
    }

    public boolean isLight() {
        return isLight;
    }

    public Light turnOn() {
        isLight = true;
        return this;
    }
}
