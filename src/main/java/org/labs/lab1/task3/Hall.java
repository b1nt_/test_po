package org.labs.lab1.task3;

import java.util.ArrayList;

/**
 * Created by Слава on 20.03.2017.
 */
public class Hall extends Location {
    private Thing[] things;
    private Door[] doors;
    private Light light;
    private ArrayList<Person> guests = new ArrayList<Person>();

    public void setThings(Thing[] things) {
        this.things = things;
    }

    public void setDoors(Door[] doors) {
        this.doors = doors;
    }

    public void setLight(Light light) {
        this.light = light;
    }

    public Thing[] getThings() {
        return things;
    }

    public Door[] getDoors() {
        return doors;
    }

    public Light getLight() {
        return light;
    }

    public ArrayList<Person> getGuests() {
        return guests;
    }

    public void addGuest(Person p) {
        guests.add(p);
    }

    public void deleteGuest(Person p) {
        guests.remove(p);
    }

    public Light light(TRIP t, Light l) {
        if(TRIP.ALMOST_IMMEDIATELY.equals(t)) {
            return light.turnOn();
        }
        return null;
    }
}
