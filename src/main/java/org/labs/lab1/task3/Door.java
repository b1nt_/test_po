package org.labs.lab1.task3;

import java.util.ArrayList;

/**
 * Created by Слава on 20.03.2017.
 */
public class Door extends Location {
    private final SIDE side;
    private ArrayList<Person> passedPeople = new ArrayList<Person>();
    private final Hall location;

    public Door(SIDE side, Hall location) {
        this.side = side;
        this.location = location;
    }

    public SIDE getSide() {
        return side;
    }

    public ArrayList<Person> getPassedPeople() {
        return passedPeople;
    }

    public Hall getLocation() {
        return location;
    }

    public Location personPassed(Person p) {
        passedPeople.add(p);
        Location l = null;
        if (SIDE.FORWARD.equals(side)) {
            l = location;
            location.addGuest(p);
        }
        if (SIDE.ANOTHER_END.equals(side)) {
            location.deleteGuest(p);
        }
        return l;
    }
}
