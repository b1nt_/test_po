package org.labs.lab1.task1;

/**
 * Created by Слава on 14.03.2017.
 */
public class SuperExpMath {

    private final int n = 120;
    private final double e = 0.00001;

    public double tg(double x) {
        double flag = 0.0;

        if(x < 0) {
            flag++;
            x = Math.abs(x);
        }

        // Если угол больше 90 градусов, то необходимо свести его к углу меньшему, чем 90 градусов
        while (x - Math.PI / 2 >= e) {
            x -= Math.PI;
            if(x < 0) flag++;
        }

         //Если угол равен или кратен 90 градусам, то возвращаем бесконечность
         if(Math.abs(x - Math.PI / 2) <= e) {
             return Double.MAX_VALUE;
          }


        double result = 0.0;
        double[] fact_numbers = Fact(2 * n);
        //double[] bern_numbers = Bern(2 * n);


        /*
        for(int i = 1; i <= n; i++) {
            result += Math.abs(bern_numbers[2 * i]) * (Math.pow(2, 2* i) * (Math.pow(2, 2 * i) - 1) / fact_numbers[2 * i]) * Math.pow(Math.abs(x), 2 * i -1);
        } */

        return (flag % 2.0 != 0 ? -1.0 : 1.0 ) * result;
    }

    private static double[] Fact(int n) {
        double[] fact = new double[n + 1];
        fact[0] = 1;
        fact[1] = 1;

        double r = 1;
        for (int i = 2; i <= n; ++i) {
            r *= i;
            fact[i] = r;
        }
        return fact;
    }

    /*
    static double[] Bern(int n) {
        double[] Bernoulli_numbers = new double[n + 1];
        Bernoulli_numbers[0] = 1;

        //Цикл для суммы
        for (int i = 1; i <= n; i++) {
            // Если номер числа Бернули нечетный, то оно равно 0
            if(n % 2 != 0 && n != 1) {
                Bernoulli_numbers[i] = 0;
                continue;
            }

            double S = 0;

            //Цикл для k
            for (int k = 1; k <= i; k++) {
                double Coef = 1;

                //Цикл для биномиального коэффициента
                for (int j = 1; j <= k + 1; j++) {
                    Coef *= (double)(i - j + 2) / j;
                }
                S += Coef * Bernoulli_numbers[i - k];
            }
            Bernoulli_numbers[i] = (-S) / (i + 1);
        }
        return Bernoulli_numbers;
    } */
}
