package org.labs.lab1.task1;

/**
 * Created by Слава on 12.03.2017.
 */
public class ExpMath {
    private final int n = 120;
    private final double e = 0.00001;

    public double tg(double x) {
        double flag = 0.0;

        if(x < 0) {
            flag++;
            x = Math.abs(x);
        }

        while (x - Math.PI / 2 >= e) {
            x -= Math.PI;
            if(x < 0) flag++;
        }

        if(0 <= Math.abs(x - Math.PI / 2) && Math.abs(x - Math.PI / 2) <= e) {
            return Double.MAX_VALUE;
        }

        double result = 0.0;

        /*
        for(int i = 1; i <= n; i++) {
            result += Math.abs(Bern(2 * i)) * (Math.pow(2, 2* i) * (Math.pow(2, 2 * i) - 1) / Fact(2 * i)) * Math.pow(Math.abs(x), 2 * i -1);
        } */

        return (flag % 2.0 != 0 ? -1.0 : 1.0 ) * result;
    }


    /*
    private static double Bern(int n) {
        if(n == 0) {
            return 1.0;
        }
        if(n % 2 != 0 && n != 1) {
            return 0;
        }

        double result = 0.0;
        for(int k = 1; k <= n; k++) {
            result +=  (Fact(n + 1) / (Fact(n - k) * Fact(k+1))) * Bern(n - k);
        }
        return  (-1.0 / (n + 1)) * result;
    } */


    /*
    public static double Fact(int n) {
        double r = 1;
        for (int i = 2; i <= n; ++i) {
            r *= i;
        }
        return r;
    } */


    /*
    public static double Bern(int n) {
        double[] Bernoulli_numbers = new double[n + 1];
        Bernoulli_numbers[0] = 1;
        double Bernoulli = 1;

        //Цикл для суммы
        for (int i = 1; i <= n; i++) {

            if(n % 2 != 0 && n != 1) {
                Bernoulli_numbers[i] = 0;
                continue;
            }

            double S = 0;

            //Цикл для k
            for (int k = 1; k <= i; k++) {
                double Coef = 1;

                //Цикл для биномиального коэффициента
                for (int j = 1; j <= k + 1; j++) {
                    Coef *= (double)(i - j + 2) / j;
                }
                S += Coef * Bernoulli_numbers[i - k];
            }
            Bernoulli = (-S) / (i + 1);
            Bernoulli_numbers[i] = Bernoulli;
        }
        return Bernoulli;
    } */
}
