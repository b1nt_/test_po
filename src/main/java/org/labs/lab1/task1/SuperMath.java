package org.labs.lab1.task1;

/**
 * Created by Слава on 11.03.2017.
 */
public class SuperMath {

    private final double e = 0.00001;

    public double tg(double x) {
        double flag = 0.0;

        if(x < 0) {
            flag++;
            x = Math.abs(x);
        }

        // Если угол больше 90 градусов, то необходимо свести его к углу меньшему, чем 90 градусов
        while (x - Math.PI / 2 > e) {
            x -= Math.PI;
            if(x < 0) flag++;
        }

        // Если угол равен или кратен 90 градусам, то возвращаем бесконечность
        if(Math.abs(x - Math.PI / 2) < e) {
            return (flag % 2.0 != 0 ? -1.0 : 1.0 ) * Double.MAX_VALUE;
        }

        double result = 0.0;
        double temp_result = 1.0;
        int k = 0;

        while (Math.abs(temp_result - result) > e) {
            k++;
            temp_result = result;
            result += Math.abs(Bern(2 * k)) * (Math.pow(2, 2 * k) * (Math.pow(2, 2 * k) - 1) / Fact(2 * k)) * Math.pow(Math.abs(x), 2 * k -1);
        }

        return (flag % 2.0 != 0 ? -1.0 : 1.0 ) * result;
    }

    public static double Fact(int n) {
        double r = 1;
        for (int i = 2; i <= n; ++i) {
            r *= i;
        }
        return r;
    }

    public static double Bern(int n) {
        double[] Bernoulli_numbers = new double[n + 1];
        Bernoulli_numbers[0] = 1;
        double Bernoulli = 1;

        //Цикл для суммы
        for (int i = 1; i <= n; i++) {

            if(n % 2 != 0 && n != 1) {
                Bernoulli_numbers[i] = 0;
                continue;
            }

            double S = 0;

            //Цикл для k
            for (int k = 1; k <= i; k++) {
                double Coef = 1;

                //Цикл для биномиального коэффициента
                for (int j = 1; j <= k + 1; j++) {
                    Coef *= (double)(i - j + 2) / j;
                }
                S += Coef * Bernoulli_numbers[i - k];
            }
            Bernoulli = (-S) / (i + 1);
            Bernoulli_numbers[i] = Bernoulli;
        }
        return Bernoulli;
    }
}
